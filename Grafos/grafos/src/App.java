import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        ListaAdyacencia listaAdyacencia = new ListaAdyacencia();
        int V = 6;
        ArrayList<ArrayList<Integer>> adj = new ArrayList<ArrayList<Integer>>(V);

        for (int i = 0; i < V; i++) {
            adj.add(new ArrayList<Integer>());
        }
        listaAdyacencia.addEdge(adj, 0, 1);
        listaAdyacencia.addEdge(adj, 1, 0);
        listaAdyacencia.addEdge(adj, 1, 3);
        listaAdyacencia.addEdge(adj, 2, 3);
        listaAdyacencia.addEdge(adj, 2, 4);
        listaAdyacencia.addEdge(adj, 3, 1);
        listaAdyacencia.addEdge(adj, 3, 2);
        listaAdyacencia.addEdge(adj, 4, 2);
        

        listaAdyacencia.printGraph(adj);
    }
    /*
     * public static void main(String[] args) throws Exception { MatrizAdyacencia
     * matriz = new MatrizAdyacencia(6);
     * 
     * matriz.agregar(0, 1);
     * 
     * matriz.agregar(1, 0); matriz.agregar(1, 3);
     * 
     * matriz.agregar(2, 3); matriz.agregar(2, 4);
     * 
     * matriz.agregar(3, 1); matriz.agregar(3, 2);
     * 
     * matriz.agregar(4, 2);
     * 
     * matriz.agregar(5);
     * 
     * matriz.imprimir(); }
     */
}
