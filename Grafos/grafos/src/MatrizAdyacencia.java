public class MatrizAdyacencia {

    private int numNodos;
    private int[][] matriz;
    
    /**
    * Constructor
    * @param numNodos numero de nodos
    **/
    public MatrizAdyacencia(int numNodos) {
        this.numNodos = numNodos;
        matriz = new int[this.numNodos][this.numNodos];
        for(int i=0; i< numNodos; i++){
            for(int j=0; j< numNodos; j++){
                matriz[i][j] = 0;
            }            
        }
    }
    
    /**
    * Agregar valor segun aparece
    * @param i posicion i de la matriz
    * @param j posicion j de la matriz
    **/
    public void agregar(int i, int j){
        matriz[i][j] = matriz[i][j] + 1;
    }
    
    /**
    * imprimir matriz con valores
    * @param i posicion i de la matriz
    *  @param j posicion j de la matriz
    **/
    public void imprimir(){
        for(int i=0; i< numNodos; i++){
            for(int j=0; j< numNodos; j++){
                System.out.print( matriz[i][j] + "  " );        
            }
            System.out.println();
        }  
    }

    /**
    * Agregar valor segun aparece, este metodo solo por si acaso para verificar sus numeros se conviertan a 0
    * @param i posicion i de la matriz
    **/
	public void agregar(int i) {
        for (int j = 0; j < matriz.length; j++) {
            matriz[i][j] = 0;
        }
	}
}