import java.util.Scanner;

public class AbstractNames {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        int n;
        n = scan.nextInt();
        names(n);
        scan.close();
    }

    static void names(int n) {
        for (int i = 0; i < n; i++) {
            String name1 = scan.next();
            String name2 = scan.next();
            System.out.println(compareNames(name1, name2));
        }
    }

    static String compareNames(String name1, String name2) {
        if (name1.length() != name2.length()) {
            return "No";
        } else {
            for (int i = 0; i < name1.length(); i++) {
                if (!isVowel(name1.charAt(i))) {
                    if (name1.charAt(i) != name2.charAt(i)) {
                        return "No";
                    }
                } else {
                    if (!isVowel(name2.charAt(i))) {
                        return "No";
                    }
                }
            }
            return "Yes";
        }
    }

    private static boolean isVowel(char charAt) {
        if(charAt == 'a' || charAt == 'e' || charAt == 'i' || charAt == 'o' || charAt == 'u') {
            return true;
        } else {
            return false;
        }
    }
}
