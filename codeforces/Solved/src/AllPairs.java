import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class AllPairs {
    public static int[][] matDist;
    public static boolean[][] matInfinity;
    public static int n;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        do {
            StringTokenizer st = new StringTokenizer(br.readLine());
            n = Integer.parseInt(st.nextToken());
            int m = Integer.parseInt(st.nextToken());
            int q = Integer.parseInt(st.nextToken());
            if(n == 0 && m == 0 && q == 0){
                System.exit(0);
            } else {
                matDist = new int[n][n];
                for (int[] row: matDist)
                    Arrays.fill(row, (int) 1e9);
                matInfinity = new boolean[n][n];
                for (int i = 0; i < n; i++) {
                    matDist[i][i] = 0;
                }
                for (int i = 0; i < m; i++) {
                    StringTokenizer st1 = new StringTokenizer(br.readLine());
                    int u = Integer.parseInt(st1.nextToken());
                    int v = Integer.parseInt(st1.nextToken());
                    int w = Integer.parseInt(st1.nextToken());
                    matDist[u][v] = Math.min(matDist[u][v], w);
                }
                floydWarshall();
                for (int i = 0; i < q; i++) {
                    StringTokenizer st2 = new StringTokenizer(br.readLine());
                    int u = Integer.parseInt(st2.nextToken());
                    int v = Integer.parseInt(st2.nextToken());
                    if (matInfinity[u][v]){
                        System.out.println("-Infinity");
                    } else if(matDist[u][v] == 1e9){
                        System.out.println("Impossible");
                    } else {
                        System.out.println(matDist[u][v]);
                    }
                }
                System.out.println();
            }
        } while(true);
    }

    public static void floydWarshall(){
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < n; k++) {
                    if (matDist[j][i] < 1e9 && matDist[i][k] < 1e9){
                        matDist[j][k] = Math.min(matDist[j][k], matDist[j][i] + matDist[i][k]);
                    }
                }
            }
        }
        for (int i = 0; i < n; i++) {
            if (matDist[i][i] < 0){
                for (int j = 0; j < n; j++) {
                    for (int k = 0; k < n; k++) {
                        if (matDist[j][i] < 1e9 && matDist[i][k] < 1e9){
                           matInfinity[j][k] = true;
                        }
                    }
                }
            }
        }
    }
}
