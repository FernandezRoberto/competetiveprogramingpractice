import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class AssigningTeams {
    public static int[] list;
    public static void main(String[]args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str;
        while((str=br.readLine())!=null && str.length()!=0){
            list = new int[4];
            StringTokenizer st = new StringTokenizer(str);
            list[0] = Integer.parseInt(st.nextToken());
            list[1] = Integer.parseInt(st.nextToken());
            list[2] = Integer.parseInt(st.nextToken());
            list[3] = Integer.parseInt(st.nextToken());
            Arrays.sort(list);
            System.out.println(Math.abs((list[1]-list[0])-(list[3]-list[2])));
        }

    }
}
