import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Badge {

    public static int[] list;
    public static int[] auxList;
    public static int ans = 0;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int students = Integer.parseInt(br.readLine());
        list = new int[students];
        auxList = new int[students];
        StringTokenizer st = new StringTokenizer(br.readLine());
        StringBuilder aux = new StringBuilder();
        for (int i = 0; i < students; i++) {
            list[i] = Integer.parseInt(st.nextToken())-1;
        }
        for (int i = 0; i < students; i++) {
            dfs(i);
            aux.append(ans + 1).append(" ");
            auxList = new int[students];
        }
        System.out.println(aux);
    }

    private static void dfs(int i) {
        auxList[i] = 1;
        if (auxList[list[i]] == 0){
            auxList[list[i]] = 1;
            dfs(list[i]);
        } else {
            ans = list[i];
        }
    }
}
