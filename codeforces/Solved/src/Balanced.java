import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Balanced {
    public static char[] list;
    public static ArrayList<Character> vowels = new ArrayList<>(Arrays.asList('a', 'e', 'i', 'o', 'u', 'y'));
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int num = Integer.parseInt(st.nextToken());
        for (int i = 0; i < num; i++) {
            String str = br.readLine();
            list = str.toCharArray();
            System.out.println("String #"+(i+1)+": "+solve());
            System.out.println();
        }
    }
    public static long solve() {

        long ans = 1;
        long count = 0;
        boolean flagFirst = true;
        boolean isVowel = true;
        int first = -1;

        for (int i = 0; i < list.length; i++) {
            if(list[i] != '?'){
                if(vowels.contains(list[i])){
                    if (isVowel && first != -1){
                        return 0;
                    } else {
                        isVowel = true;
                    }
                    if (first == -1){
                        first = 1; //the first letter is a vowel;
                        flagFirst = false;
                        isVowel = true;
                    }
                } else {
                    if (!isVowel){
                        return 0;
                    } else {
                        isVowel = false;
                    }
                    if (first == -1){
                        first = 0; //the first letter is a consonant;
                        flagFirst = false;
                        isVowel = false;
                    }
                }
            } else {
                if(flagFirst) {
                    count++;
                } else {
                    if(isVowel){
                        isVowel = false;
                        ans *= 20;
                    } else {
                        isVowel = true;
                        ans *= 6;
                    }
                }

            }
        }

        if (first == -1){
            return fact(20, count / 2) * fact(6, (count+1) / 2) + fact(6, count / 2) * fact(20,
                    (count+1) / 2);
        }

        if(count>0){
            if(count%2==0){
                ans *= (count/2)* 20L * (count/2)* 6L;
            } else {
                if(first == 0){
                    ans *= fact(20, count/2) * fact(6, count/2 + 1);
                } else {
                    ans *= fact(6, count/2) * fact(20, count/2 + 1);
                }
            }
        }
        return ans;
    }

    public static long fact(long base, long exp) {
        long resp = 1;
        for (long i = 0; i < exp; i++) {
            resp *= base;
        }
        return resp;
    }
}
