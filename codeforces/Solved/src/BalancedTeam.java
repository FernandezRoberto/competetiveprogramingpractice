import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class BalancedTeam {
    public static int[] list;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int nums = Integer.parseInt(br.readLine());
        StringTokenizer st1 = new StringTokenizer(br.readLine());
        list = new int[nums];
        for (int i = 0; i < nums; i++) {
            list[i] = Integer.parseInt(st1.nextToken());
        }
        Arrays.sort(list);
        int start = 0;
        int end = 1;
        int max = 1;
        int ans = 1;
        while (end<nums) {

            if (list[end] - list[start] <= 5) {
                end++;
                max++;
            } else {
                ans = Math.max(ans, max);
                max--;
                start++;
            }
        }
        System.out.println(Math.max(ans, max));
    }
}
