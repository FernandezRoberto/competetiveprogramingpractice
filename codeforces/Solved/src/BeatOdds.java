import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class BeatOdds {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            int num = Integer.parseInt(br.readLine());
            StringTokenizer st = new StringTokenizer(br.readLine());
            int even = 0;
            int odd = 0;
            for (int j = 0; j < num; j++) {
                int n = Integer.parseInt(st.nextToken());
                if ((n & 1) != 1){
                    even++;
                } else {
                    odd++;
                }
            }

            System.out.println(Math.min(even, odd));
        }
    }
}
