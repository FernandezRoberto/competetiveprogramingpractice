import java.util.Scanner;

public class BeutifulYear {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int year;
        year = scan.nextInt();
        System.out.println(beautiful(year));
        scan.close();
    }

    static int beautiful(int year) {
        boolean flag = false;
        while(flag == false){
            year++;
            String strYear = year + "";
            int cont = 0;
            for (int i = 0; i < strYear.length(); i++) {
                for (int j = i + 1; j < strYear.length(); j++) {
                    if(strYear.charAt(i) != strYear.charAt(j)) {
                        cont++;
                    }
                }
            }
            if(cont == 6) {
                flag = true;
            }
            cont = 0;
        }
        return year;
    }
}
