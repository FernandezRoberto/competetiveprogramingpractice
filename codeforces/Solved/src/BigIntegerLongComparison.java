import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.StringTokenizer;
import java.lang.String;

public class BigIntegerLongComparison {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int tests = Integer.parseInt(br.readLine());
        for (int i = 0; i < tests; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int x1 = Integer.parseInt(st.nextToken());
            int p1 = Integer.parseInt(st.nextToken());
            StringTokenizer st1 = new StringTokenizer(br.readLine());
            int x2 = Integer.parseInt(st1.nextToken());
            int p2 = Integer.parseInt(st1.nextToken());
            int aux = Math.min(p1, p2);
            double r1 = x1 * (Math.pow(10, p1-aux));
            double r2 = x2 * (Math.pow(10, p2-aux));
            if (r1 > r2){
                System.out.println(">");
            } else if (r1 == r2) {
                System.out.println("=");
            } else {
                System.out.println("<");
            }
        }
    }
}
