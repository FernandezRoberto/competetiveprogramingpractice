import java.util.Scanner;

public class BigNumberOfTeams {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        int n;
        n = scan.nextInt();
        scan.nextLine();
        names(n);
        scan.close();
    }

    static void names(int n) {
        for (int i = 0; i < n; i++) {
            String name1 = scan.nextLine();
            String name2 = scan.nextLine();
            System.out.println("Case " + (i+1) + compareNames(name1, name2));
        }
    }

    private static String compareNames(String name1, String name2) {
        if (name1.equals(name2) && name1.length() == name2.length()) {
            return ": Yes";
        } else if (wordMissMatch(name1, name2)) {
            return ": Output Format Error";
        } else {
            return ": Wrong Answer";
        }
    }

    private static boolean wordMissMatch(String name1, String name2) {
        String[] words = name1.split(" ");
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < words.length; i++) {
            sb.append(words[i]);
         }
         String str = sb.toString()
            .replace(",", "")  //remove the commas
            .replace("[", "")  //remove the right bracket
            .replace("]", "")  //remove the left bracket
            .replace(" ", "")
            .trim(); 
        if(str.equals(name2)) {
            return true;
        } else {
            return false;
        }
        
    }
}
