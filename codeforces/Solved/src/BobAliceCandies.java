import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class BobAliceCandies {
    public static ArrayList<Integer> list = new ArrayList<>();
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            int candies = Integer.parseInt(br.readLine());
            StringTokenizer st = new StringTokenizer(br.readLine());
            for (int j = 0; j < candies; j++) {
                list.add(Integer.parseInt(st.nextToken()));
            }
            solve();
        }
    }

    private static void solve() {
        int turns = 0;
        int biggest = 0;
        int alice = 0;
        int bob = 0;
        boolean turn = true;
        while(true){
            if(list.isEmpty()){
                break;
            } else {
                if (turn){
                    int sum = 0;
                    while(true){
                        if (list.isEmpty()){
                            break;
                        }
                        if(sum > biggest){
                            biggest = sum;
                            break;
                        } else {
                            sum = sum + list.remove(0);
                        }
                    }
                    alice = alice + sum;
                    turn = false;
                    turns++;
                } else {
                    int sum = 0;
                    while(true){
                        if (list.isEmpty()){
                            break;
                        }
                        if(sum > biggest){
                            biggest = sum;
                            break;
                        } else {
                            sum = sum + list.remove(list.size()-1);
                        }
                    }
                    bob = bob + sum;
                    turn = true;
                    turns++;
                }
            }
        }
        System.out.println(turns + " " + alice + " " + bob);
    }
}
