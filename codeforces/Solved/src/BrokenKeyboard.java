import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

public class BrokenKeyboard {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {
        String str;
        while ((str = br.readLine()) != null) {
            keyboard(str);
        }
    }

    static void keyboard(String str) {
        LinkedList<Character> value = new LinkedList<>();
        int pointer = 0;
        for (char character : str.toCharArray()) {
            if (character == '[') {
                pointer = 0;
            }
            else if (character == ']') {
                pointer = value.size();
            }
            else {
                value.add(pointer++, character);
            }
        }

        StringBuilder sb = new StringBuilder();
        for (char character : value) {
            sb.append(character);
        }
        System.out.println(sb.toString());
    }
}
