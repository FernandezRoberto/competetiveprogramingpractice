import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class CD {
    public static int[] list;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        do{
            int sum = 0;
            StringTokenizer st = new StringTokenizer(br.readLine());
            int n = Integer.parseInt(st.nextToken());
            list = new int[n];
            int m = Integer.parseInt(st.nextToken());
            if (n == 0 && m ==0){
                System.exit(0);
            }
            for (int i = 0; i < n; i++) {
                list[i] = Integer.parseInt(br.readLine());
            }
            for (int i = 0; i < m; i++) {
                if (Arrays.binarySearch(list, Integer.parseInt(br.readLine())) >= 0){
                    sum++;
                }
            }
            System.out.println(sum);
        } while(true);
    }
}
