import java.util.Scanner;

public class Cakeminator {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        int r, c;
        r = scan.nextInt();
        c = scan.nextInt();
        char[][] matriz = new char[r][c];
        matriz = fillMatriz(r,c);
        System.out.println(minator(matriz));
        scan.close();
    }

    static int minator(char[][] matriz) {
        int cont = 0;
        int colums = 0;
        int sum = 0;
        while(cont < matriz.length) {
            if (verifiColumn(matriz, cont)) {
                sum = sum + matriz[0].length;
                colums++;
            }
            cont++;
        }
        cont = 0;
        while(cont < matriz[0].length) {
            if (verifiRow(matriz, cont)) {
                sum = sum + (matriz.length - colums);
            }
            cont++;
        }
        return sum;
    }

    static boolean verifiColumn(char[][] matriz, int cont) {
        for (int i = 0; i < matriz[1].length; i++) {
            if (matriz[cont][i] == 'S'){
                return false;
            }
        }
        return true;
    }

    static boolean verifiRow(char[][] matriz, int cont) {
        for (int i = 0; i < matriz.length; i++) {
            if (matriz[i][cont] == 'S'){
                return false;
            }
        }
        return true;
    }

    static char[][] fillMatriz(int c, int r) {
        char[][] matriz = new char[c][r];
        for (int i = 0; i < c; i++) {
            String pal = scan.next();
            for (int j = 0; j < r; j++) {
                matriz[i][j] = pal.charAt(j);
            }
        }
        return matriz;
    }
}
