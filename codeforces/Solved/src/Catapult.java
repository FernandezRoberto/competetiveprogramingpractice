import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Catapult {
    public static int[] list;
    public static int[] sparseList;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st1 = new StringTokenizer(br.readLine());
        int nums = Integer.parseInt(st1.nextToken());
        int queries = Integer.parseInt(st1.nextToken());
        StringTokenizer st2 = new StringTokenizer(br.readLine());
        list = new int[nums];
        sparseList = new int[nums];
        for (int i = 0; i < nums; i++) {
            list[i] = Integer.parseInt(st2.nextToken());
        }
        construct();
        int ans = 0;
        for (int i = 0; i < queries; i++) {
            StringTokenizer st3 = new StringTokenizer(br.readLine());
            int start = Integer.parseInt(st3.nextToken());
            int end = Integer.parseInt(st3.nextToken());
            if (end >= start){
                if( sparseList[start-1]+start+1 >= end){
                    ans++;
                    //System.out.println(start+"entra"+end);
                }
            }

        }
        System.out.println(ans);
    }

    private static void construct() {
        for (int i = 0; i < list.length; i++) {
            int num = 0;
            for (int j = i+1; j < list.length; j++) {
                if(list[i]<list[j]){
                    sparseList[i] = num;
                    break;
                } else {
                    num++;
                }
            }
            if (i== list.length-1){
                sparseList[i] = num;
            }
        }
    }
}
