import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Catsage {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int year = Integer.parseInt(st.nextToken());
        int month = Integer.parseInt(st.nextToken());
        int humanYear = 0;
        int humanMonth = 0;
        if (year == 0){
            humanMonth = month * 15;
        } else if (year < 2) {
            humanYear = 15;
            humanMonth = month * 9;
        } else {
            humanYear = 15 + 9 + ((year - 2) * 4);
            humanMonth = month * 4;
        }
        int years = humanMonth / 12;
        humanYear += years;
        humanMonth = humanMonth - (years * 12);
        System.out.println(humanYear +" "+humanMonth);
    }

}
