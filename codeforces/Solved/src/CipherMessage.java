import java.util.ArrayList;
import java.util.Scanner;

public class CipherMessage {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str;
        str = scan.next();
        message(str);
        scan.close();
    }

    static void message(String str) {
        ArrayList<Character> chars = new ArrayList<>();
        for (int i = 0; i < str.length(); i++) {
            chars.add(str.charAt(i));
        }
        for (int i = 0; i < chars.size() - 1; i++) {
            if(chars.get(i) == chars.get(i+1)) {
                chars.remove(i+1);
                chars.remove(i);
                i = -1;
            }
        }
        //String result = chars.stream().map(e->e.toString()).collect(Collectors.joining());
        for (int i = 0; i < chars.size(); i++) {
            System.out.print(chars.get(i));
        }
    }
}