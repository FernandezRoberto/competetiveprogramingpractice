package CodeForcesContest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Bit {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        int x = 0;
        for (int i = 0; i < cases; i++) {
            String operation = br.readLine();
            if(operation.equals("++X") || operation.equals("X++")) {
                x++;
            } else {
                x--;
            }
        }
        System.out.println(x);
    }
}
