package CodeForcesContest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HelpfulMaths {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String word = br.readLine();
        String[] numbers = word.split("\\+");
        for (int i = 0; i < numbers.length ; i++) {
            for (int j = 0; j < numbers.length; j++) {
                if(Integer.parseInt(numbers[i]) < Integer.parseInt(numbers[j])) {
                    int aux = Integer.parseInt(numbers[i]);
                    numbers[i] = numbers[j];
                    numbers[j] = aux+"";
                }
            }
        }
        String resp = "";
        for (int i = 0; i < numbers.length - 1; i++) {
            resp = resp + numbers[i] + "+";
        }
        System.out.println(resp + numbers[numbers.length - 1]);
    }
}
