package CodeForcesContest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class NextRound {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st1 = new StringTokenizer(br.readLine());
        int cant = Integer.parseInt(st1.nextToken());
        int position = Integer.parseInt(st1.nextToken());
        StringTokenizer st = new StringTokenizer(br.readLine());
        int[] nums = new int[cant];
        int cont = 0;
        int value = Integer.MIN_VALUE;
        for (int i = 0; i < cant; i++) {
            int x = Integer.parseInt(st.nextToken());
            if(i>=position-1){
                if(i==position-1){value = x;}
                if(x >= value && x != 0){
                    cont++;
                }
            } else {
                nums[i] = x;
            }
        }
        for (int i : nums) {
            if(i >= value && i != 0) {
                cont++;
            }
        }
        System.out.println(cont);
    }
}
