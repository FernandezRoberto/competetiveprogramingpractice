package CodeForcesContest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PetyaAndStrings {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String word1 = br.readLine().toLowerCase();
        String word2 = br.readLine().toLowerCase();
        if(word1.compareToIgnoreCase(word2) == 0) {
            System.out.println(0);
        } else if(word1.compareToIgnoreCase(word2) < 0) {
            System.out.println(-1);
        } else {
            System.out.println(1);
        }
    }
}
