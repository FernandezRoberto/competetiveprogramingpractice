package CodeForcesContest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class SpecialPermutation {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            int size = Integer.parseInt(br.readLine());
            ArrayList<Integer> array = new ArrayList<Integer>();
            for (int j = 0; j < size; j++) {
                array.add(j+1);
            }
            
            array.add(0,array.remove(array.size()-1)); 
            for (int j = 0; j < array.size(); j++) {
                if (j == array.size()-1) {
                    System.out.println(array.get(j));
                } else {
                    System.out.print(array.get(j) + " ");
                }

            }

        }
    }
}
