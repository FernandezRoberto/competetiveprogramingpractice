package CodeForcesContest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
//import java.util.HashMap;
import java.util.StringTokenizer;

public class UniqueBidAction {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            int size = Integer.parseInt(br.readLine());
            ArrayList<Integer> arrays= new ArrayList<>();
            StringTokenizer st = new StringTokenizer(br.readLine());
            int menor = Integer.MAX_VALUE;
            for (int j = 0; j < size; j++) {
                int num = Integer.parseInt(st.nextToken());
                arrays.add(num);
            }
            for (int j = 0; j < arrays.size(); j++) {
                if(arrays.contains(arrays.get(j))){
                    //arrays.removeAll(arrays.get(j));
                }
            }
            if(menor==Integer.MAX_VALUE) {
                System.out.println(-1);
            } else{
                System.out.println(arrays.indexOf(menor)+1);
            }
            
        }
    }
}
