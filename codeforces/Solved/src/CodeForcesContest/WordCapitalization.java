package CodeForcesContest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class WordCapitalization {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String word = br.readLine();
        String firstWord = word.substring(0, 1).toUpperCase() + word.substring(1, word.length());
        System.out.println(firstWord);
    }
}
