package CodeMonk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class MonkAndRotations {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int size = Integer.parseInt(st.nextToken());
            int rotates = Integer.parseInt(st.nextToken());
            ArrayList<Integer> array = new ArrayList<Integer>();
            StringTokenizer st1 = new StringTokenizer(br.readLine());
            for (int j = 0; j < size; j++) {
                array.add(Integer.parseInt(st1.nextToken()));
            }
            List<Integer> newArray = array.subList(0, array.size()-rotates);
            newArray.addAll(0,array.subList(array.size()-rotates, array.size()));
            for (int k=0; k<newArray.size();k++) {
                if(newArray.size()-1 == k){
                    System.out.print(newArray.get(k));
                } else {
                    System.out.print(newArray.get(k) + " ");
                }
                
            }
            System.out.println();
        }
    }
}
