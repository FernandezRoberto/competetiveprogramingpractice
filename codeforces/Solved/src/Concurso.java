import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class Concurso {
    public static TreeMap<Integer, String> map = new TreeMap<>();
    public static TreeMap<String, Integer> map2 = new TreeMap<>();
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        for (int i = 0; i < n; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            String str = st.nextToken();
            int num = Integer.parseInt(st.nextToken());
            map.put(num, str);
        }
        StringBuilder sb = new StringBuilder();
        int ans = 1;
        if(map.size()<3){
            for (int i = 0; i < map.size(); i++) {
                int key = map.lastKey();
                String value = map.get(key);
                map2.put(value, key);
                ans = ans * key;
                map.remove(key);
                i--;
            }
            for (int i = 0; i < map.size(); i++) {
                String key = map2.firstKey();
                int value = map2.get(key);
                sb.append(key).append(" ");
                map2.remove(key);
                i--;
            }
        } else {
            for (int i = 0; i < 3; i++) {
                int key = map.lastKey();
                String value = map.get(key);
                map2.put(value, key);
                ans = ans * key;
                map.remove(key);
            }
            for (int i = 0; i < 3; i++) {
                String key = map2.firstKey();
                int value = map2.get(key);
                sb.append(key).append(" ");
                map2.remove(key);
            }
        }
        System.out.println(sb.toString() + ans);

    }
}
