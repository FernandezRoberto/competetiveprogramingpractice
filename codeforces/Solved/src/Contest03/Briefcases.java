import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicInteger;

public class Briefcases {
    public static void main(String[] args) throws Exception {
        final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        final BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        StringTokenizer st = new StringTokenizer(br.readLine());
        Map<Integer, Integer> map = new LinkedHashMap<>();
        map.put(1, Integer.parseInt(st.nextToken()));
        map.put(5, Integer.parseInt(st.nextToken()));
        map.put(10, Integer.parseInt(st.nextToken()));
        map.put(20, Integer.parseInt(st.nextToken()));
        map.put(50, Integer.parseInt(st.nextToken()));
        map.put(100, Integer.parseInt(st.nextToken()));
        AtomicInteger gK = new AtomicInteger(Integer.MIN_VALUE);
        AtomicInteger gV = new AtomicInteger(Integer.MIN_VALUE);
        map.forEach((k, v) -> {
            int val = k * v;
            if (val >= gV.get()) {
                gV.set(val);
                gK.set(k);
            }
        });
        bw.write(gK.toString());
        bw.newLine();
        bw.flush();
    }
}