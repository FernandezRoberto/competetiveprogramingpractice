package Contest03;

import java.io.*;
import java.util.StringTokenizer;

public class Corrupt {
    public static void main(String[] args) throws IOException {
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input = br.readLine();
        StringTokenizer st = new StringTokenizer(input, " ");
        int teams = Integer.parseInt(st.nextToken());
        int problems = Integer.parseInt(st.nextToken());
        int lastPenalty = 0;
        int actualPenalty = 0;
        StringBuilder res = new StringBuilder();
        boolean ambiguos = false;
        boolean zeros = false;
        if (teams >= problems) {
            for (int i = 0; i < teams; i++) {
                actualPenalty = Integer.parseInt(br.readLine());
                if (i != 0) {
                    if (actualPenalty < lastPenalty) {
                        problems--;
                        if (problems < 0) {
                            ambiguos = true;
                            break;
                        }
                    } else if (actualPenalty == lastPenalty && actualPenalty != 0) {
                        ambiguos = true;
                        break;
                    } else if (lastPenalty == 0 && actualPenalty > 0) {
                        ambiguos = true;
                        break;
                    }
                }
                if (actualPenalty == 0)
                    problems = 0;
                lastPenalty = actualPenalty;
                res.append(problems).append('\n');
            }
        } else {
            ambiguos = true;
        }

        if (ambiguos) {
            bw.write("ambiguous");
            bw.newLine();
        } else  {
            bw.write(res.toString());
        }
        bw.flush();
    }
}