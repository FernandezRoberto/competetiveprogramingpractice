package Contest03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class Divvying {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int contest = Integer.parseInt(br.readLine());
        StringTokenizer st = new StringTokenizer(br.readLine());
        int total = 0;
        for (int i = 0; i < contest; i++) {
            total = total + Integer.parseInt(st.nextToken());
        }
        if(total % 3 == 0){
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }
}
