
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.StringTokenizer;

public class Mind {
    public static void main(String[] args) throws Exception {
        final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        final BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int n = Integer.parseInt(br.readLine());
        Map<Character, Queue<Integer>> map = new HashMap<>();
        int current = 'A';
        for (int i = 0; i < n; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int m = Integer.parseInt(st.nextToken());
            Queue<Integer> q = new LinkedList<>();
            for (int j = 0; j < m; j++) {
                q.add(Integer.parseInt(st.nextToken()));
            }
            map.put((char)current, q);
            current++;
        }
        boolean allEmpty = false;
        while (!allEmpty) {
            allEmpty = true;
            char minK = ' ';
            int minVal = Integer.MAX_VALUE;
            Queue<Integer> minV = null;
            for (Map.Entry<Character, Queue<Integer>> a : map.entrySet()) {
                Queue<Integer> v = a.getValue();
                char label = a.getKey();
                if (!v.isEmpty()) {
                    allEmpty = false;
                    if (minVal > v.peek()) {
                        minVal = v.peek();
                        minV = v;
                        minK = label;
                    }
                }
            }
            if (minV != null)
                minV.poll();
            if(minK != ' '){
                bw.write(minK);
            }
        }
        bw.newLine();
        bw.flush();
    }
}