
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;

public class Moderate {
    public static void main(String[] args) throws IOException {
        final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        final BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        int n = Integer.parseInt(br.readLine());
        StringTokenizer one = new StringTokenizer(br.readLine());
        StringTokenizer two = new StringTokenizer(br.readLine());
        StringTokenizer three = new StringTokenizer(br.readLine());
        String median = Integer.toString(median(Integer.parseInt(one.nextToken()), Integer.parseInt(two.nextToken()), Integer.parseInt(three.nextToken())));
        bw.write(median);
        for (int i = 1; i < n; i++) {
            bw.write(' ');
            median = Integer.toString(median(Integer.parseInt(one.nextToken()), Integer.parseInt(two.nextToken()), Integer.parseInt(three.nextToken())));
            bw.write(median);
        }
        bw.newLine();
        bw.flush();

    }

    private static int median(int one, int two, int three) {
        if ((one <= two && two <= three) || (three <= two && two <= one)) {
            return two;
        } else if ((two <= one && one <= three) || (three <= one && one <= two)) {
            return one;
        }
        return three;
    }
}