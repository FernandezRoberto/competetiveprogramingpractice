package Contest03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class ProblemB {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int number = Integer.parseInt(br.readLine());
        StringTokenizer st = new StringTokenizer(br.readLine());
        while(number > 0 ){
            int n = Integer.parseInt(st.nextToken());
            int k = Integer.parseInt(st.nextToken());
            String word = br.readLine();
            if(word.chars().filter(ch -> ch == '*').count() == 1){
                System.out.println(1);
            } else if (word.chars().filter(ch -> ch == '*').count() == 2){
                System.out.println(2);
            } else {
                ArrayList<Integer> list = new ArrayList<>();
                for (int i = 0; i < word.length(); i++) {
                    if(word.charAt(i) == '*'){
                        list.add(i);
                    }
                }
                int count = 0;
                int lastKey = 0;
                int anotherKey = 0;
                for (int num:list) {
                    if(count == 0) {
                        lastKey = num;
                        count++;
                    } else if(num - lastKey < k){
                        anotherKey = num;
                    } else if(num - lastKey > k){
                        lastKey = anotherKey;
                        count++;
                    }else if(num - lastKey == k){
                        lastKey = num;
                        count++;
                    }
                }
                System.out.println(count);
            }
            number--;
        }
    }
}
