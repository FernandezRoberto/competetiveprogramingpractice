package Contest03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Root {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int number = Integer.parseInt(br.readLine());
        if(Math.sqrt(number) % 1 == 0){
            System.out.println((int)Math.floor(Math.sqrt(number)));
        } else {
            System.out.println(number);
        }
    }
}
