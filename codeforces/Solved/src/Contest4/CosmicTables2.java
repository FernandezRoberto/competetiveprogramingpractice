package Contest4;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
 
public class CosmicTables2 {
    static FastScanner fscan = new FastScanner(System.in);
    static BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
 
    public static void main(String[] args) throws IOException {
        int n = fscan.nextInt();
        int m = fscan.nextInt();
        int k = fscan.nextInt();
        int[][] mat = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                int a = fscan.nextInt();
                mat[i][j] = a;
            }
        }
        cosmic(mat, k);
    }
    static void cosmic(int[][] mat, int k) throws IOException {
        for (int i = 0; i < k; i++) {
            char letter = (char)fscan.nextString().charAt(0);
            int first = fscan.nextInt();
            int secound = fscan.nextInt();
            if (letter == 'g') {
				bw.write(mat[first-1][secound-1] + "\n");
            } else if (letter == 'r') {
                mat = changeRows(mat, first, secound);
            } else {
                mat = changeColumns(mat, first, secound);
            }
		}
		bw.close();
    }
    private static int[][] changeRows(int[][] mat, int first, int secound) {
        for (int i = 0; i < mat[0].length; i++) {
            int aux = mat[first-1][i];
            mat[first-1][i] = mat[secound-1][i];
            mat[secound-1][i] = aux;
        }
        return mat;
    }
 
    private static int[][] changeColumns(int[][] mat, int first, int secound) {
        for (int i = 0; i < mat.length; i++) {
            int aux = mat[i][first-1];
            mat[i][first-1] = mat[i][secound-1];
            mat[i][secound-1] = aux;
        }
        return mat;
    }
}

