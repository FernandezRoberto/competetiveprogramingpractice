package Contest4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GoodTeacher {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cant = Integer.parseInt(br.readLine());
        StringTokenizer st = new StringTokenizer(br.readLine());
        String[] array = new String[cant];
        for (int k = 0; k < cant; k++) {
            array[k] = st.nextToken();
        }
        int numeros = Integer.parseInt(br.readLine());
        for (int i = 0; i < numeros; i++) {
            int numero = Integer.parseInt(br.readLine()) - 1;
            if(!array[numero].equals("?")) {
                System.out.println(array[numero]);
            } else {
                boolean flag = false;
                int left = numero - 1;
                int rigth = numero + 1;
                int cont = 1;
                while (!flag) {
                    if(rigth < cant && left >= 0){
                        if (!array[left].equals("?") && !array[rigth].equals("?")) {
                            System.out.println("middle of " + array[left] + " and " + array[rigth]);
                            flag = true;
                            break;
                        } else if (!array[left].equals("?") && array[rigth].equals("?")){
                            System.out.println(repeat("right of ", cont) + array[left]);
                            flag = true;
                            break;
                        } else if (array[left].equals("?") && !array[rigth].equals("?")) {
                            System.out.println(repeat("left of ", cont) + array[rigth]);
                            flag = true;
                            break;
                        } else {
                            cont++;
                            left--;
                            rigth++;
                        }
                    } else {
                        if(rigth >= cant && left >=0) {
                            if (!array[left].equals("?")) {
                                System.out.println(repeat("right of ", cont) + array[left]);
                                flag = true;
                                break;
                            } else {
                                left--;
                                cont++;
                            }
                        }
                        if(left < 0 && rigth < cant) {
                            if (!array[rigth].equals("?")) {
                                System.out.println(repeat("left of ", cont) + array[rigth]);
                                flag = true;
                                break;
                            } else {
                                rigth++;
                                cont++;
                            }
                        }
                    }
                }
            }
        }
    }

    public static String repeat(String s, int n) {
        return Stream.generate(() -> s).limit(n).collect(Collectors.joining(""));
    }
}