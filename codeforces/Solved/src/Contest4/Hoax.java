package Contest4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.TreeMap;
public class Hoax {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int days = Integer.parseInt(br.readLine());
		while (days != 0) {
			long cost = 0;
			TreeMap<Integer, Integer> map = new TreeMap<>();
			for (int i = 0; i < days; i++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				int bills = Integer.parseInt(st.nextToken());
				for (int j = 0; j < bills; j++) {
					int amount = Integer.parseInt(st.nextToken());
					if(map.containsKey(amount)) {
						map.put(amount, map.get(amount) + 1);
					} else {
						map.put(amount, 1);
					}
					
				}
				int highest = map.lastKey();
				int lowest = map.firstKey();
				if (map.get(highest) == 1) {
					map.remove(highest);
				} else {
					map.put(highest, map.get(highest) - 1);
				}
				if (map.get(lowest) == 1) {
					map.remove(lowest);
				} else {
					map.put(lowest, map.get(lowest) - 1);
				}
				cost += (highest - lowest);
			}
			System.out.println(cost);
			days = Integer.parseInt(br.readLine());
		}
	}
}
