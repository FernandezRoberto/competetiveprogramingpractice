package Contest4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Scanner;

public class LemmingsBatle {
    public static void main(String[] args) {
        PriorityQueue<Integer> queueGreen = new PriorityQueue<>(Collections.reverseOrder());
        PriorityQueue<Integer> queueBlue = new PriorityQueue<>(Collections.reverseOrder());
        Scanner scan = new Scanner(System.in);
        int cant = scan.nextInt();
        for(int i = 0; i<cant; i++){
            int battles = scan.nextInt();
            int sGreen = scan.nextInt();
            int sBlue = scan.nextInt();
            for (int j = 0; j < sGreen; j++) {
                queueGreen.add(scan.nextInt());
            }
            for (int j = 0; j < sBlue; j++) {
                queueBlue.add(scan.nextInt());
            }
            int cont = 0;
            
            for (int j = 0; j < battles; j++) {
                ArrayList<Integer> greenArray = new ArrayList<>();
                ArrayList<Integer> blueArray = new ArrayList<>();
                for (int k = 0; k < (sGreen > sBlue ? sGreen : sBlue); k++) {
                    if(queueBlue.isEmpty() && queueGreen.isEmpty()) {
                        System.out.println("green and blue died\n");
                        j = battles;
                        break;
                    } else if (queueBlue.isEmpty()) {
                        System.out.println("green wins");
                        while (!queueGreen.isEmpty()) {
                            System.out.println(queueGreen.poll());
                        }
                        System.out.println();
                        j = battles;
                        break;
                    } else if (queueGreen.isEmpty()) {
                        System.out.println("blue wins");
                        while (!queueBlue.isEmpty()) {
                            System.out.println(queueBlue.poll());
                        }
                        System.out.println();
                        j = battles;
                        break;
                    } else {
                        
                        if(cont < battles) {
                            int a = queueBlue.poll();
                            int b = queueGreen.poll();
                            if(a>b) {
                                blueArray.add(a-b);
                            } else if(b>a) {
                                greenArray.add(b-a);
                            }
                            cont++;
                        }
                        
                    }
                }
                queueBlue.addAll(blueArray);
                queueGreen.addAll(greenArray);

            }
        }
        scan.close();
    }
}
