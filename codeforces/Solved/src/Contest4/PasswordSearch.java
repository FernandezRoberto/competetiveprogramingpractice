package Contest4;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Hashtable;

public class PasswordSearch {
    static FastScanner fscan = new FastScanner(System.in);
    static BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
 
    public static void main(String[] args) throws IOException {
        int n = fscan.nextInt();
        String letter = fscan.nextString();
        int size = letter.length() - (n-1);
        Hashtable<Integer, String> hashTable = new Hashtable<Integer, String>();
        for (int i = 0; i < size; i++) {
           hashTable.put(i, letter.substring(i, i + n));
        }
        search(hashTable);
    }
    
    private static void search(Hashtable<Integer, String> hashTable) throws IOException {
        int size = hashTable.size();
        int cont = 0;
        int actual = 0;
        String word = hashTable.get(0);
        String comparator = "";
        for (int i = 0; i < size; i++) {
            if(cont > actual) {
                actual = cont;
                word = comparator;
            }
             comparator = hashTable.get(i);
            
            hashTable.remove(i);
            cont=1;
            if(size!=0) {
                for (int j = i+1; j < size; j++) {
                    if(comparator.equals(hashTable.get(j))){
                        cont++;
                    }
                }
            }
        }
        bw.write(word+"\n");
        bw.close();
    }
}