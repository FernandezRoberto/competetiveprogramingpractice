package Contest4;

import java.io.BufferedWriter;
import java.io.IOException;
//import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

public class PasswordSearch2 {
    static FastScanner fscan = new FastScanner(System.in);
    static BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
    static ArrayList<String> list = new ArrayList<String>();

    public static void main(String[] args) throws IOException {
        int n = fscan.nextInt();
        String letter = fscan.nextString();
        int size = letter.length() - (n - 1);
        for (int i = 0; i < size; i++) {
            list.add(letter.substring(i, i + n));
        }
        search();
    }
    
    private static void search() throws IOException {
        Set<String> st = new HashSet<String>(list); 
        Hashtable<Integer, String> hashTable = new Hashtable<Integer, String>();
        int key=-1;
        for (String s : st) { 
            int num = Collections.frequency(list, s);
            hashTable.put(num, s);
            if(key<num) {
                key = num;
            }
        }
        bw.write(hashTable.get(key)+"\n");
        bw.close();
    }
}

/*class FastScanner {
    InputStream is;
    byte buff[] = new byte[1024];
    int currentChar = -1;
    int buffChars = 0;

    public FastScanner(InputStream inputStream) {
        is = inputStream;
    }

    public boolean hasNext() throws IOException {
        return buffChars >= 0;
    }

    public int nextChar() throws IOException {
        // if we already have that next char read, just return else input
        if (currentChar == -1 || currentChar >= buffChars) {
            currentChar = 0;
            buffChars = is.read(buff);
        }

        if (buffChars <= 0)
            return -1;

        return (char) buff[currentChar++];
    }

    public String nextString() throws IOException {
        StringBuilder bldr = new StringBuilder();
        int ch;
        while (isSpace(ch = nextChar()))
            ;

        do {
            bldr.append((char) ch);
        } while (!isSpace(ch = nextChar()));

        return bldr.toString();
    }

    public int nextInt() throws IOException {
        // considering ASCII files--> 8 bit chars, unicode files has 16 bit chars (byte1
        // then byte2)
        int result = 0;
        int sign = 1;
        int ch;
        while (isSpace(ch = nextChar()))
            ;

        if (ch == '-') {
            sign = -1;
            ch = nextChar();
        }

        do {
            if (ch < '0' || ch > '9')
                throw new NumberFormatException("Found '" + ch + "' while parsing for int.");

            result *= 10;
            result += ch - '0';
        } while (!isSpace(ch = nextChar()));

        return sign * result;
    }

    public long nextLong() throws IOException {
        // considering ASCII files--> 8 bit chars, unicode files has 16 bit chars (byte1
        // then byte2)
        long result = 0;
        int sign = 1;
        int ch;
        while (isSpace(ch = nextChar()))
            ;

        if (ch == '-') {
            sign = -1;
            ch = nextChar();
        }

        do {
            if (ch < '0' || ch > '9')
                throw new NumberFormatException("Found '" + ch + "' while parsing for int.");

            result *= 10;
            result += ch - '0';
        } while (!isSpace(ch = nextChar()));

        return sign * result;
    }

    private boolean isSpace(int ch) {
        return ch == '\n' || ch == ' ' || ch == '\t' || ch == '\r' || ch == -1;
    }

    public void close() throws IOException {
        is.close();
    }
}*/