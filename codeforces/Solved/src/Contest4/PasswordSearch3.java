package Contest4;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Scanner;

public class PasswordSearch3 {
    public static void main(String[] args) throws IOException {
        Scanner fscan = new Scanner(System.in);
        while (fscan.hasNext()) {
            Hashtable<String, Integer> hashTable = new Hashtable<String, Integer>();
            int n = fscan.nextInt();
            String letter = fscan.next();
            for (int i = 0; i < letter.length() - n + 1; i++) {
                String word = letter.substring(i, i + n);
                if (hashTable.containsKey(word)) {
                    hashTable.put(word, hashTable.get(word) + 1);
                } else {
                    hashTable.put(word, 1);
                }
            }
            Integer max = Integer.MIN_VALUE;
            String word = "";
            for (String k : hashTable.keySet()) {
                Integer value = hashTable.get(k);
                if (value > max) {
                    max = value;
                    word = k; 
                }
            }
            System.out.println(word);
        }
        fscan.close();
    }

    
}
