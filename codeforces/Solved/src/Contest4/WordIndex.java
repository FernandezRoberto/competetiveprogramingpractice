package Contest4;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
 
public class WordIndex {
 
    static HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
 
    public static void main(String[] args) {
 
        generatee();
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            Integer a = hashMap.get(in.next());
            System.out.println(a != null ? a : 0);
        }
        in.close();
    }
 
    private static void generatee() {
 
        int cont = 1;
        Queue<String> queue = new LinkedList<String>();
 
        for (Character character = 'a'; character <= 'z'; character++) {
            queue.add(character + "");
            hashMap.put(character + "", cont++);
        }
        while (!queue.isEmpty()) {
            String s = queue.poll();
            char last = s.charAt(s.length() - 1);
            if (s.length() == 5 || last == 'z') {
                continue;
            }
            for (char next = ++last; next <= 'z'; next++){
                hashMap.put(s + next, cont++);
                queue.add(s + next);
            }
        }
 
    }
}