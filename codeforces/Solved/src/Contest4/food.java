package Contest4;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Stack;
import java.util.StringTokenizer;

public class food {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cant = Integer.parseInt(br.readLine());
        StringTokenizer st = new StringTokenizer(br.readLine());
        HashMap<Integer, Integer> grupo = new HashMap<Integer, Integer>();
        Stack<Integer> stack = new Stack<Integer>();
        stack.add(Integer.parseInt(st.nextToken()));
        for (int k = 0; k < cant - 1; k++) {
            int num = Integer.parseInt(st.nextToken());
            if(grupo.containsKey(num)) {
                grupo.put(num, grupo.get(num) + 1);
            }
            if(stack.isEmpty() && !grupo.containsKey(num)) {
                stack.push(num);
            } else if(!grupo.containsKey(num)) {
                if(num == stack.peek()) {
                    grupo.put(num, num);
                    stack.pop();
                } else {
                    grupo.put(stack.peek(), stack.peek());
                    stack.push(num);
                }
            }
        }
        int cont = 0;
        while(stack.isEmpty()) {
            if(grupo.get(stack.peek()) == 1) {
                stack.pop();
            } else {
                cont++;
                stack.pop();
            }
        }
        System.out.println(cont);
        
    }
}
