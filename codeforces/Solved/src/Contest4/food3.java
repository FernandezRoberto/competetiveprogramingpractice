package Contest4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.StringTokenizer;

public class food3{
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cant = Integer.parseInt(br.readLine());
        StringTokenizer st = new StringTokenizer(br.readLine());
        HashMap<Integer, Integer> grupo = new HashMap<Integer, Integer>();
        HashMap<Integer, Integer> desechos = new HashMap<Integer, Integer>();
        int last = -1;
        int cont = 0;
        for (int k = 0; k < cant; k++) {
            int num = Integer.parseInt(st.nextToken());
            if(last == -1) {
                last = num;
            } else if(last == num){
                grupo.put(last, last);
                last = -1;
            } else if(!grupo.containsKey(num) && !desechos.containsKey(num)){
                if(!grupo.containsKey(last) && !desechos.containsKey(last)){
                    cont++;
                }
                desechos.put(last, last);
                last = num;
                
            }
        }
        System.out.println(cont);
    }
}
