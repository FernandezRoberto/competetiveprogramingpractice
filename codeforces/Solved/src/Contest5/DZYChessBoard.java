package Contest5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class DZYChessBoard {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int x = Integer.parseInt(st.nextToken());
        int y = Integer.parseInt(st.nextToken());
        char[][] mat = new char[x][y];
        for (int i = 0; i < x; i++) {
           String pal = br.readLine();
            boolean flag = true;
            for (int j = 0; j < y; j++) {
                if(pal.charAt(j)=='.'){
                    if(i%2==0 && flag) {
                        mat[i][j] = 'B';
                        flag = false;
                    } else if(i%2==0 && !flag) {
                        mat[i][j] = 'W';
                        flag = true;
                    } else if(i%2!=0 && flag) {
                        mat[i][j] = 'W';
                        flag = false;
                    } else if(i%2!=0 && !flag) {
                        mat[i][j] = 'B';
                        flag = true;
                    }
                } else {
                    mat[i][j] = '-';
                    if(flag){
                        flag = false;
                    } else {
                        flag = true;
                    }
                }
            }
        }
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                System.out.print(mat[i][j]);
            }
            System.out.println();
        }
    }
}
