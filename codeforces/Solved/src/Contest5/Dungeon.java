package Contest5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Dungeon {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cant = Integer.parseInt(br.readLine());
        for (int i = 0; i < cant; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            long x = Integer.parseInt(st.nextToken());
            long y = Integer.parseInt(st.nextToken());
            long z = Integer.parseInt(st.nextToken());
            boolean finished = true;
            long count = 1;
            if(x+y+z < 7) {
                System.out.println("NO");
                finished = false;
            } else {
                while (finished) {
                    if(x == 0 || y == 0 || z == 0) {
                        System.out.println("NO");
                        finished = false;
                    } else {
                        if(count%7 == 0) {
                            x--;
                            y--;
                            z--;
                            count++;
                            if(x==0 && y == 0 && z == 0) {
                                System.out.println("YES");
                                finished = false;
                            }
                        } else {
                            if(x >= y && x >= z && z >= y) {
                                if(x==z){
                                    x--;
                                    count ++;
                                } else {
                                    count = count + (x - z);
                                    x = x - (x - z);
                                    
                                }
                                
                            } else if(x >= y && x >= z && y >= z) {
                                if(x == y){
                                    x--;
                                    count ++;
                                } else {
                                    count = count + (x - y);
                                    x = x - (x - y);
                                    
                                }
                                
                            }else if(y >= x && y >= z && x >= z) {
                                if(y==x){
                                    y--;
                                    count++;
                                } else {
                                    count = count + (y - x);
                                    y = y - (y - x);
                                    
                                }
                                
                            } else if(y >= x && y >= z && z >= x) {
                                if(y==z){
                                    y--;
                                    count++;
                                }else{
                                    count = count + (y - z);
                                    y = y - (y - z);
                                    
                                }
                                
                            } else if(z >= x && z >= y && x >= y) {
                                if(z==x){
                                    z--;
                                    count++;
                                } else {
                                    count = count + (z - x);
                                    z = z - (z - x);
                                }
                                
                            } else {
                                if(z==y){
                                    z--;
                                    count++;
                                } else {
                                    count = count + (z - y);
                                    z = z - (z - y);
                                    
                                }
                                
                            }
                        }
                    }
                }
            }
            
        }
    }
}
