package Contest5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ShortestPathKing {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String st = br.readLine();
        String st1 = br.readLine();
        char iniX = st.charAt(0);
        int iniY = Integer.parseInt(st.charAt(1)+"");
        char finX = st1.charAt(0);
        int finY = Integer.parseInt(st1.charAt(1)+"");
        int res = iniX - finX;
        int res2 = iniY - finY;
        if(res<0){
            res*=-1;
        }
        if(res2<0){
            res2*=-1;
        }
        if(res>res2){
            System.out.println(res);
        } else {
            System.out.println(res2);
        }
        while(iniX != finX || iniY != finY) {
            if(iniX <= finX && iniY <= finY){
                if(iniX<finX) {
                    System.out.print("R");
                    iniX++;
                }
                if(iniY < finY) {
                    System.out.print("U");
                    iniY++;
                }
                System.out.println();
            } else if(iniX >= finX && iniY >= finY){
                if(iniX>finX) {
                    System.out.print("L");
                    iniX--;
                }
                if(iniY > finY) {
                    System.out.print("D");
                    iniY--;
                }
                System.out.println();
            } else if(iniX >= finX && iniY <= finY){
                if(iniX>finX) {
                    System.out.print("L");
                    iniX--;
                }
                if(iniY < finY) {
                    System.out.print("U");
                    iniY++;
                }
                System.out.println();
            } else if(iniX <= finX && iniY >= finY){
                if(iniX<finX) {
                    System.out.print("R");
                    iniX++;
                }
                if(iniY > finY) {
                    System.out.print("D");
                    iniY--;
                }
                System.out.println();
            }
        }
    }
}
