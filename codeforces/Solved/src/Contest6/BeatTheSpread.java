package Contest6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class BeatTheSpread {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cant = Integer.parseInt(br.readLine());
        for (int i = 0; i < cant; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int x = Integer.parseInt(st.nextToken());
            int y = Integer.parseInt(st.nextToken());
            int num = (x - y) / 2;
            int num2 = x - num;
            if(num + num2 == x && (num - num2 == y || num2 - num == y) && num >= 0 && num2 >= 0){
                if(num > num2) {
                    System.out.println(num +" "+num2);
                } else {
                    System.out.println(num2 +" "+num);
                }
            } else {
                System.out.println("impossible");
            }
        }
    }
}
