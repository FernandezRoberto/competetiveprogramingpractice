package Contest6;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class DreamoonStairs {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int x = Integer.parseInt(st.nextToken());
        int y = Integer.parseInt(st.nextToken());
        int low = x/2;
        int res = -1;
        if(x%2==1){
            low++;
        }
        for (int i = low; i < x; i++) {
            if(i%y==0){
                res = i;
                i = x;
            }
        }
        System.out.println(res);
    }
}
/*BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int n = Integer.parseInt(st.nextToken());
        int m = Integer.parseInt(st.nextToken());
        int low=n/2;
        int res=-1;
        if(n%2==1){
            low++;
        }
        for(int i=low;i<=n;i++){
            if(i%m==0){
               res=i; break;
            }
        }
        System.out.println(res);*/