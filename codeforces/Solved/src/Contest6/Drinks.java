package Contest6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Drinks {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cant = Integer.parseInt(br.readLine());
        double resp = 0;
        int suma = 0;
        double sumaFrac = 0;
        StringTokenizer st = new StringTokenizer(br.readLine());
        for (int i = 0; i < cant; i++) {
            int x = Integer.parseInt(st.nextToken());
            suma = suma + x;
            double r = (double)x/100;
            sumaFrac = sumaFrac + r;
        }
        resp = sumaFrac * suma;
        sumaFrac = cant * suma;
        resp = resp / sumaFrac;
        if(Double.isNaN(resp)) {
            System.out.println(String.format("%.12f", 0.000000000000));
        } else {
            System.out.println(String.format("%.12f", resp*100));
        }
        
        
    }
}
