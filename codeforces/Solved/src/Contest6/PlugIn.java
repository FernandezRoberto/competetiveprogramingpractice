package Contest6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

public class PlugIn {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str = br.readLine();
        Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < str.length(); i++) {
            if(stack.isEmpty()) {
                stack.add(str.charAt(i));
            } else if(stack.peek() == str.charAt(i)) {
                stack.pop();
            } else {
                stack.add(str.charAt(i));
            }
        }
        System.out.print(stack.toString().replaceAll("\\[", "").replaceAll("]", "").replaceAll(", ", ""));
    }
}
