package Contest6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class TPrimes {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cant = Integer.parseInt(br.readLine());
        StringTokenizer st = new StringTokenizer(br.readLine());
        for (int i = 0; i < cant; i++) {
            long x = Long.parseLong(st.nextToken());
            int cont = 0;
            for (long j = 1; j <= x; j++) {
                if(cont > 3) {
                    j = x;
                    cont = 0;
                } else {
                    if(x%j == 0) {
                        cont++;
                    }
                }
            }
            if(cont == 3){
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }
}
