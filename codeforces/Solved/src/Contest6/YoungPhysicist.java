package Contest6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class YoungPhysicist {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cant = Integer.parseInt(br.readLine());
        int resp1 = 0;
        int resp2 = 0;
        int resp3 = 0;
        for (int i = 0; i < cant; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int x = Integer.parseInt(st.nextToken());
            int y = Integer.parseInt(st.nextToken());
            int z = Integer.parseInt(st.nextToken());
            resp1 = resp1 + x;
            resp2 = resp2 + y;
            resp3 = resp3 + z;
        }
        if(resp1 == 0 && resp2 == 0 && resp3 == 0){
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
        
    }
}
