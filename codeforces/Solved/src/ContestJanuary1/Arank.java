
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Arank {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int teams = Integer.parseInt(st.nextToken());
        int matches = Integer.parseInt(st.nextToken());
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 1; i <= teams; i++) {
            list.add(i);
        }
        for (int i = 0; i < matches; i++) {
            StringTokenizer match = new StringTokenizer(br.readLine());
            String Swinner = match.nextToken();
            String Sloser = match.nextToken();
            int winner = Integer.parseInt(String.valueOf(Swinner.charAt(1)));
            int loser = Integer.parseInt(String.valueOf(Sloser.charAt(1)));
            list = revalidatePositions(winner, loser, list);
        }
        for (int i = 0; i < list.size()-1; i++) {
            System.out.print("T"+list.get(i)+" ");
        }
        System.out.println("T"+list.get(list.size()-1));
    }

    private static ArrayList<Integer> revalidatePositions(int winner, int loser, ArrayList<Integer> list) {
        int posWinner = getPosition(winner, list);
        int posLoser = getPosition(loser, list);
        if(posWinner > posLoser) {
            list.add(posWinner+1, loser);
            list.remove(posLoser);

        }
        return list;
    }

    private static int getPosition(int number, ArrayList<Integer> list) {
        return list.indexOf(number);
    }
}
