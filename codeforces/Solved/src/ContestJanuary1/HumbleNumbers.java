package ContestJanuary1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class HumbleNumbers {
    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        while(n != 0){
            BigInteger[] seq = humble(1_000_000);
            System.out.println(seq[n-1]);
            n = Integer.parseInt(br.readLine());
        }
    }

    private static BigInteger[] humble(int n) {
        BigInteger two = BigInteger.valueOf(2);
        BigInteger twoTest = two;
        BigInteger three = BigInteger.valueOf(3);
        BigInteger threeTest = three;
        BigInteger five = BigInteger.valueOf(5);
        BigInteger fiveTest = five;
        BigInteger seven = BigInteger.valueOf(7);
        BigInteger sevenTest = seven;
        BigInteger[] results = new BigInteger[n];
        results[0] = BigInteger.ONE;
        int twoIndex = 0, threeIndex = 0, fiveIndex = 0, sevenIndex = 0;
        for (int index = 1; index < n; index++) {
            results[index] = twoTest.min(threeTest).min(fiveTest).min(sevenTest);
            if (results[index].compareTo(twoTest) == 0) {
                twoIndex++;
                twoTest = two.multiply(results[twoIndex]);
            }
            if (results[index].compareTo(threeTest) == 0) {
                threeIndex++;
                threeTest = three.multiply(results[threeIndex]);
            }
            if (results[index].compareTo(fiveTest) == 0) {
                fiveIndex++;
                fiveTest = five.multiply(results[fiveIndex]);
            }
            if (results[index].compareTo(sevenTest) == 0) {
                sevenIndex++;
                sevenTest = seven.multiply(results[sevenIndex]);
            }
        }
        return results;
    }
}
