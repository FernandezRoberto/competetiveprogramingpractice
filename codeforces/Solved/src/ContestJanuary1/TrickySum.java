package ContestJanuary1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TrickySum {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cant = Integer.parseInt(br.readLine());
        for (int i = 0; i < cant; i++) {
            long num = Integer.parseInt(br.readLine());
            long aux = 1;
            long sum = 0;
            sum = (num * (num + 1)) / 2;
            while(aux <= num) {
                aux = aux * 2;
            } 
            sum = sum - aux * 2 + 2;
            System.out.println(sum);
        }
    }

}
