import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class CraneAndTurtle {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int animals = Integer.parseInt(st.nextToken());
        int legs = Integer.parseInt(st.nextToken());
        int ani = 0;
        if(legs % 2 != 0){
            System.out.println("No");
        } else {
            if(legs > 2){
                ani = legs / 4;
                legs = legs - (ani * 4);
            }
            if(legs == 2){
                ani = ani + (legs / 2);
            }
            if (ani == animals){
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        }
    }
}
