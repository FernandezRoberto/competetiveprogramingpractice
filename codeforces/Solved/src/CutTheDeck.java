import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CutTheDeck {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine());
        for (int i = 0; i < num; i++) {
            String str = br.readLine();
            System.out.println(solve(str));
        }
    }

    private static int solve(String str) {
        int b = 0;
        int r = 0;
        int ans = 0;
        boolean flag = true;
        for (int i = 0; i < str.length(); i++) {
            if(str.charAt(i) == 'B'){
                b++;
            } else {
                if(r == b && str.length()-1 != i){
                    ans = i+1;
                    if (flag){
                        str = str + str.substring(0, i+1);
                    } else {
                        str = str + str.charAt(i);
                    }
                    flag = false;
                } else {
                    r++;
                }
            }
        }
        if(r==b){
            return ans;
        }
        return -1;
    }
}