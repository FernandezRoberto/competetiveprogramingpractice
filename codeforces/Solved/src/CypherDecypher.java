import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class CypherDecypher {

     static class SegmentTree {
        int[] st;
        SegmentTree(int[] arr, int n) {
            int x = (int) (Math.ceil(Math.log(n) / Math.log(2)));
            int max_size = 2 * (int) Math.pow(2, x) - 1;
            st = new int[max_size];
            constructSTUtil(arr, 0, n - 1, 0);
        }
        int getMid(int s, int e) {
            return s + (e - s) / 2;
        }

        int getSumUtil(int ss, int se, int qs, int qe, int si) {
            if (qs <= ss && qe >= se)
                return st[si];
            if (se < qs || ss > qe)
                return 0;
            int mid = getMid(ss, se);
            return getSumUtil(ss, mid, qs, qe, 2 * si + 1) +
                    getSumUtil(mid + 1, se, qs, qe, 2 * si + 2);
        }

        int getSum(int n, int qs, int qe) {
            return getSumUtil(0, n - 1, qs, qe, 0);
        }

        int constructSTUtil(int[] arr, int ss, int se, int si) {
            if (ss == se) {
                st[si] = arr[ss];
                return arr[ss];
            }
            int mid = getMid(ss, se);
            st[si] = constructSTUtil(arr, ss, mid, si * 2 + 1) +
                    constructSTUtil(arr, mid + 1, se, si * 2 + 2);
            return st[si];
        }
    }

    static int[] prime = new int[1000000+1];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine());
        criba(1000000);
        SegmentTree tree = new SegmentTree(prime, prime.length);
        for (int i = 0; i < num; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int a = Integer.parseInt(st.nextToken());
            int b = Integer.parseInt(st.nextToken());
            if (a<2){
                a=2;
            }
            System.out.println(tree.getSum(prime.length, a, b));
        }
    }

    public static void criba(int b) {
        for(int i=0; i< prime.length;i++) {
            prime[i] = 1;
        }
        for(int p = 2; p < Math.sqrt(b); p++) {
            if(prime[p] == 1) {
                for(int i = p*p; i <= b; i = i + p) {
                    prime[i] = 0;
                }
            }
        }
    }
}
