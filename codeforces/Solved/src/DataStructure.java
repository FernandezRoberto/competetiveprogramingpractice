import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

public class DataStructure {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {
        String str;
        while ((str = br.readLine()) != null && !str.equals("")) {
            structure(Integer.parseInt(str));
        }
    }

    static void structure(int number) throws IOException {
        Stack<Integer> stack = new Stack<Integer>();
        Queue<Integer> queue = new LinkedList<Integer>();
        PriorityQueue<Integer> pq = new PriorityQueue<Integer>(number, Collections.reverseOrder());
        boolean isStack = true;
        boolean isQueue = true;
        boolean isPQ = true;
        for (int i = 0; i < number; i++) {
            String input = br.readLine();
            String[] inputs = input.split(" ");
            int n1 = Integer.parseInt(inputs[0]);
            int n2 = Integer.parseInt(inputs[1]);
            if(n1 == 1) {
                stack.push(n2);
                queue.add(n2);
                pq.add(n2);
            } else {
                if(isStack) {
                    if(stack.isEmpty()) {
                        isStack = false;
                    }else if(stack.peek() == n2) {
                        stack.pop();
                    } else {
                        isStack = false;
                    }
                }
                if(isQueue) {
                    if(queue.isEmpty()) {
                        isQueue = false;
                    }else if(queue.peek() == n2) {
                        queue.poll();
                    } else {
                        isQueue = false;
                    }
                }
                if(isPQ) {
                    if(pq.isEmpty()) {
                        isPQ = false;
                    } else if(pq.peek() == n2) {
                        pq.poll();
                    } else {
                        isPQ = false;
                    }
                }
            }
        }
        if(isStack && !isQueue && !isPQ) {
            System.out.println("stack");
        } else if(!isStack && !isQueue && !isPQ) {
            System.out.println("impossible");
        } else if (!isStack && isQueue && !isPQ) {
            System.out.println("queue");
        } else if (!isStack && !isQueue && isPQ) {
            System.out.println("priority queue");
        } else {
            System.out.println("not sure");
        }   
    }
}
