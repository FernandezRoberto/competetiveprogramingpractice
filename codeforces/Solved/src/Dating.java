import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringTokenizer;

public class Dating {

    public static Double[] list;
    public static void main(String[]args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str;
        while((str=br.readLine())!=null && str.length()!=0) {
            int cases = Integer.parseInt(str);
            list = new Double[cases];
            StringTokenizer st = new StringTokenizer(br.readLine());
            for (int i = 0; i < cases; i++) {
                list[i] = Double.parseDouble(st.nextToken());
            }
            Arrays.sort(list, Collections.reverseOrder());
            double score = 0;
            double right = list[0];
            double left = list[0];
            for (int i = 1; i < cases; i++) {
                if (left > right) {
                    double temp = left;
                    left = right;
                    right = temp;
                }
                score = score + right * list[i];
                right = list[i];
            }
            score = score + left * right;
            System.out.printf("%.3f", score * Math.sin(2 * Math.PI / cases) / 2);
            System.out.println();
        }
    }
}
