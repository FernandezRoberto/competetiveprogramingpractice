import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Deletion {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            boolean flag = true;
            String a = br.readLine();
            char b = br.readLine().charAt(0);
            if (a.length()==1){
                System.out.println(a.charAt(0) == b?"Yes":"No");
            } else {
                for (int j = 0; j < a.length(); j++) {
                    if (a.charAt(j) == b && j%2==0){
                        System.out.println("YES");
                        flag = false;
                        break;
                    }
                }
                if (flag)
                    System.out.println("NO");
            }
        }
    }
}
