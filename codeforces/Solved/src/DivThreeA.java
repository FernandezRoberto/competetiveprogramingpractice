import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class DivThreeA {
    public static int[][] pascalThree;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine());
        pascalThree = new int[num][num];
        pascalThree[0][0] = 1;
        for (int i = 1; i < num; i++) {
            for (int j = 0; j <= i; j++) {
                if(j-1>=0 && pascalThree[i-1][j]!=0){
                    pascalThree[i][j] = pascalThree[i-1][j-1] + pascalThree[i-1][j];
                } else {
                    pascalThree[i][j] = 1;
                }
            }
        }

        for (int i = 0; i < num; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(pascalThree[i][j] + " ");
            }
            System.out.println();
        }
    }
}
