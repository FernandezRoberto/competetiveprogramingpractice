import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class DividingCandy {

    public static int[] list;
    public static void main(String[]args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        list = new int[n + 1];
        int[] comp = new int[1000007];
        StringTokenizer st = new StringTokenizer(br.readLine());
        int cont = 0;
        for (int i = 1; i <= n; i++) {
            list[i] = Integer.parseInt(st.nextToken());
            comp[list[i]] = comp[list[i]] + 1;
        }
        if (n==1){
            System.out.println("N");
            System.exit(0);
        }
        for (int i = 0; i < 1000000; i++) {
            if (comp[i]%2 == 1){
                cont++;
            }
            comp[i+1] = comp[i+1] + comp[i] / 2;
        }
        if (cont<=2){
            System.out.println("Y");
        } else {
            System.out.println("N");
        }
    }
}
