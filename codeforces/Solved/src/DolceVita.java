import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class DolceVita {
    public static long[] list;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            long n = Long.parseLong(st.nextToken());
            long x = Long.parseLong(st.nextToken());
            StringTokenizer st1 = new StringTokenizer(br.readLine());
            list = new long[(int) n];
            for (int j = 0; j < list.length; j++) {
                list[j] = Long.parseLong(st1.nextToken());
            }
            Arrays.sort(list);
            solve(x);
        }
    }

    private static void solve(long x) {
        long cont = 0;
        long adding = 0;
        while(true){
            long sum = 0;
            long verifier = 0;
            for (int i = 0; i < list.length; i++) {
                if(sum + (list[i]+adding) <= x){
                    cont++;
                    sum = sum + (list[i]+adding);
                } else {
                    break;
                }
            }
            if(sum==verifier){
                break;
            }
            adding++;
        }
        System.out.println(cont);
    }
}
