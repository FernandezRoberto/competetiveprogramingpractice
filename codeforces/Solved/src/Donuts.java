import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Donuts {
    public static void main(String[]args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        String str = br.readLine();
        String[] list = str.split("\\.");
        int money =Integer.parseInt(list[1]);
        int late = 0;
        for (int i = 0; i < cases; i++) {
            String str1 = br.readLine();
            String[] list1 = str1.split("\\.");
            int money1 = Integer.parseInt(list1[1]);
            money = money + money1;
            if(money > 100){
                late++;
                money = money - 100;
            } else if (money == 100 ){
                money = money - 100;
            } else if(money > 0 && money < 100){
                late++;
            }
        }
        System.out.println(late);
    }
}
