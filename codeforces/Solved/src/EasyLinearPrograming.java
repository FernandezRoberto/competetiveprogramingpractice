import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class EasyLinearPrograming {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int a = Integer.parseInt(st.nextToken());
        int b = Integer.parseInt(st.nextToken());
        int c = Integer.parseInt(st.nextToken());
        int k = Integer.parseInt(st.nextToken());
        int sum = 0;
        if (k>a){
            k = k - a;
            sum = a;
        } else {
            sum = k;
            System.out.println(sum);
            System.exit(0);
        }
        if(k>b){
            k = k - b;
        } else {
            System.out.println(sum);
            System.exit(0);
        }
        if (k>=c){
            k = k - c;
            sum = sum - c;
        } else {
            sum = sum - k;
            System.out.println(sum);
            System.exit(0);
        }
        System.out.println(sum);
    }
}
