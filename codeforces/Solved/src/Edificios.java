import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Edificios {
    long[] st;
    Edificios(long[] arr, long n) {
        long x = (long) (Math.ceil(Math.log(n) / Math.log(2)));
        int max_size = 2 * (int) Math.pow(2, x) - 1;
        st = new long[max_size];
        constructSTUtil(arr, 0, n - 1, 0);
    }

    long getMid(long s, long e) {
        return s + (e - s) / 2;
    }

    long getSumUtil(long ss, long se, long qs, long qe, long si) {
        if (qs <= ss && qe >= se)
            return st[(int) si];
        if (se < qs || ss > qe)
            return 0;
        long mid = getMid(ss, se);
        return getSumUtil(ss, mid, qs, qe, 2 * si + 1) +
                getSumUtil(mid + 1, se, qs, qe, 2 * si + 2);
    }

    void updateValueUtil(long ss, long se, long i, long diff, long si) {
        if (i < ss || i > se)
            return;
        st[(int) si] = st[(int) si] + diff;
        if (se != ss) {
            long mid = getMid(ss, se);
            updateValueUtil(ss, mid, i, diff, 2 * si + 1);
            updateValueUtil(mid + 1, se, i, diff, 2 * si + 2);
        }
    }

    void updateValue(long[] arr, long n, long i, long new_val) {
        long diff = new_val - arr[(int) i];
        arr[(int) i] = new_val;
        updateValueUtil(0, n - 1, i, diff, 0);
    }

    long getSum(long n, long qs, long qe) {
        return getSumUtil(0, n - 1, qs, qe, 0);
    }

    long constructSTUtil(long[] arr, long ss, long se, long si) {
        if (ss == se) {
            st[(int) si] = arr[(int) ss];
            return arr[(int) ss];
        }
        long mid = getMid(ss, se);
        st[(int) si] = constructSTUtil(arr, ss, mid, si * 2 + 1) +
                constructSTUtil(arr, mid + 1, se, si * 2 + 2);
        return st[(int) si];
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int num1 = Integer.parseInt(st.nextToken());
        int num2 = Integer.parseInt(st.nextToken());
        long[] arr = new long[num1];
        StringTokenizer st1 = new StringTokenizer(br.readLine());
        for (int i = 0; i < num1; i++) {
            arr[i] = Integer.parseInt(st1.nextToken());
        }
        long n = arr.length;
        Edificios  tree = new Edificios(arr, n);
        for (int i = 0; i < num2; i++) {
            StringTokenizer st2 = new StringTokenizer(br.readLine());
            char car = st2.nextToken().charAt(0);
            int n1 = Integer.parseInt(st2.nextToken());
            int n2 = Integer.parseInt(st2.nextToken());
            if (car == 'q'){
                System.out.println(tree.getSum(n, n1 - 1, n2 - 1));
            } else {
                long a = arr[n1 - 1];
                tree.updateValue(arr, n, n1 - 1, a + n2);
            }
        }
    }
}