import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Elections {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Long num = Long.parseLong(br.readLine());
        for (int i = 0; i < num; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            solve(Long.parseLong(st.nextToken()), Long.parseLong(st.nextToken()),
                    Long.parseLong(st.nextToken()));
        }
    }

    private static void solve(long n1, long n2, long n3) {
        if (n1 == n2 && n2 == n3){
            System.out.println("1 1 1");
        } else {
            long max = findMax(n1, n2, n3);
            System.out.println((max==n1?0:max-n1+1) + " " + (max==n2?0:max-n2+1) + " " + (max==n3
                    ?0:max-n3+1) );
        }
    }

    private static long findMax(long n1, long n2, long n3) {
        if(n1>n2 && n1 > n3){
            return n1;
        } else if(n2>n1 && n2>n3){
            return n2;
        } else {
            return n3;
        }
    }
}
