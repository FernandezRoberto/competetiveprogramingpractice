import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Electricity {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        long n = Long.parseLong(st.nextToken());
        long r = Long.parseLong(st.nextToken());
        long reserve = 0;
        long max = 0;
        long min = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            StringTokenizer st1 = new StringTokenizer(br.readLine());
            long s = Long.parseLong(st1.nextToken());
            long w = Long.parseLong(st1.nextToken());
            long e;
            if(i==0){
                e = s;
                if(r > w){
                    reserve = w;
                } else {
                    reserve = r;
                }
            }else if(i == n-1){
                e = s + (w + reserve);
            } else {
                if(w<r){
                    e = s + (w + reserve);
                    reserve = 0;
                } else {
                    e = s + (w + reserve - r);
                    reserve = r;
                }
            }
            if(e > max){
                max = e;
            }
            if (e < min){
                min = e;
            }
        }
        System.out.println(max-min);
    }
}
