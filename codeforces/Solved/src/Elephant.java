import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Elephant {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        double num = Integer.parseInt(br.readLine());
        System.out.println((int)Math.ceil(num/5));
    }
}
