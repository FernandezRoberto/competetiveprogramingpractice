import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class EvenOdds {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        long s = Long.parseLong(st.nextToken());
        long t = Long.parseLong(st.nextToken());
        if(((s%2==0)?(s/2):(s/2)+1) >= t){
            long res = 1;
            if (t == 1){
                System.out.println(1);
            }else if (t == 2){
                System.out.println(3);
            } else {
                System.out.println(res * (t % 2 != 0 ? t : t + 1) + 2);
            }
        } else {
            t = t - ((s%2==0)?(s/2):(s/2)+1);
            long res = 2;
            System.out.println(res*(t % 2 != 0 ? t : t + 2));
        }
    }
}
