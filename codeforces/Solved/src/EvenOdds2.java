import java.util.Scanner;

public class EvenOdds2 {
    public static Scanner scan = new Scanner(System.in);

    public static void main(String args[]) {
        long m;
        long n;
        n = scan.nextLong();
        m = scan.nextLong();
        sticks(n, m);
    }

    static public void sticks(long n, long m) {
        int cont = 0;
        int impar = 1;
        int par = 2;
        int[] resp = new int[(int) n];
        while (impar <= n) {
            resp[cont] = impar;
            impar = impar + 2;
            cont++;
        }
        while (par <= n) {
            resp[cont] = par;
            par = par + 2;
            cont++;
        }
        System.out.println(resp[(int) (m - 1)]);
    }
}