import java.util.Scanner;

public class EvensOdds {
    public static void main(String [] args)
    {
       Scanner scan = new Scanner (System.in);
       int num;
       long pos;
       num=scan.nextInt();
       pos=scan.nextLong();
       System.out.println(EvenOdds(num, pos));
       scan.close();
    }
    
    static long EvenOdds(int num, long position) {
        long[] array = new long[num];
        int cont = 0;
        long ini = 1;
        long iniPar;
        if (num % 2 == 0) {
            iniPar = num / 2;
        } else {
            iniPar = num / 2 + 1;
        }
        while (cont < num / 2) {
            array[cont] = ini;
            array[(int) iniPar] = ini + 1;
            iniPar++;
            cont++;
            ini = ini + 2;
        }

        return array[(int) (position - 1)];
    }
}
