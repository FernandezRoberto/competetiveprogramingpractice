import java.util.Scanner;

public class Expression {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a, b, c;
        a = scan.nextInt();
        b = scan.nextInt();
        c = scan.nextInt();
        System.out.println(expression(a, b, c));
        scan.close();
    }

    static int expression(int a, int b, int c) {
        int op1, op2, op3, op4, op5, op6, op7;
        op1 = a + b * c;
        op2 = a * (b + c);
        op3 = a * b * c;
        op4 = (a + b) * c;
        op5 = a + b + c;
        op6 = a * b + c;
        op7 = a * (b * c);
    
        if(op1 >= op2 && op1 >= op3 && op1 >= op4 && op1 >= op5 && op1 >= op6 && op1 >= op7) {
            return op1;
        } else if(op2 >= op1 && op2 >= op3 && op2 >= op4 && op2 >= op5 && op2 >= op6 && op2 >= op7) {
            return op2;
        } else if(op3 >= op1 && op3 >= op2 && op3 >= op4 && op3 >= op5 && op3 >= op6 && op3 >= op7) {
            return op3;
        } else if(op4 >= op1 && op4 >= op2 && op4 >= op3 && op4 >= op5 && op4 >= op6 && op4 >= op7) {
            return op4;
        } else if(op5 >= op1 && op5 >= op2 && op5 >= op3 && op5 >= op4 && op5 >= op6 && op5 >= op7) {
            return op5;
        } else if(op6 >= op1 && op6 >= op2 && op6 >= op3 && op6 >= op4 && op6 >= op5 && op6 >= op7) {
            return op6;
        } else {
            return op7;
        }
    }
}
