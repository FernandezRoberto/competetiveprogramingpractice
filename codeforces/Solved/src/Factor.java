import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.StringTokenizer;

public class Factor {

    private static final HashSet<Integer> set = new HashSet<>();

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine());
        for (int i = 0; i < num; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int n1 = Integer.parseInt(st.nextToken());
            int n2 = Integer.parseInt(st.nextToken());
            solve(n1,n2);
            System.out.println(set.size());
            set.clear();
        }
    }

    private static void solve(int n1, int n2) {
        primeFactors(n1);
        primeFactors(n2);
    }

    private static void primeFactors(int number){
        while (number%2==0) {
            set.add(2);
            number /= 2;
        }
        for (int i = 3; i <= Math.sqrt(number); i+= 2) {
            while (number%i == 0) {
                set.add(i);
                number /= i;
            }
        }
        if (number > 2) {
            set.add(number);
        }
    }
}
