import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class FarmRobot {
    public static int[] list;
    public static void main(String[]args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str;
        while((str=br.readLine())!=null && str.length()!=0){
            StringTokenizer st = new StringTokenizer(str);
            int n = Integer.parseInt(st.nextToken());
            int c = Integer.parseInt(st.nextToken());
            int s = Integer.parseInt(st.nextToken());
            list = new int[n];
            st = new StringTokenizer(br.readLine());
            solve(c, st);
            System.out.println(list[s-1]);
        }


    }

    private static void solve(int c, StringTokenizer st) {
        int pos = 0;
        list[0] = 1;
        for (int i = 0; i < c; i++) {
            int num = Integer.parseInt(st.nextToken());
            if(num == 1){
                if(pos == list.length - 1){
                    pos = 0;
                } else {
                    pos++;
                }
            } else {
                if(pos == 0){
                    pos = list.length - 1;
                } else {
                    pos--;
                }
            }
            list[pos] = list[pos] + 1;
        }
    }
}
