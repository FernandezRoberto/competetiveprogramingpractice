import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.StringTokenizer;

public class FastMultiplication {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            BigInteger num1 = new BigInteger(st.nextToken());
            BigInteger num2 = new BigInteger(st.nextToken());
            System.out.println(multiply(num1, num2));
        }
    }

    private static String multiply(BigInteger num1, BigInteger num2) {
        return num1.multiply(num2).toString();
    }
}
