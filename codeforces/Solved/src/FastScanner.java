import java.io.IOException;
import java.io.InputStream;

class FastScanner {
    InputStream is;
    byte buff[] = new byte[1024];
    int currentChar = -1;
    int buffChars = 0;

    public FastScanner(InputStream inputStream) {
        is = inputStream;
    }

    public boolean hasNext() throws IOException {
        return buffChars >= 0;
    }

    public int nextChar() throws IOException {
        // if we already have that next char read, just return else input
        if (currentChar == -1 || currentChar >= buffChars) {
            currentChar = 0;
            buffChars = is.read(buff);
        }

        if (buffChars <= 0)
            return -1;

        return (char) buff[currentChar++];
    }

    public String nextString() throws IOException {
        StringBuilder bldr = new StringBuilder();
        int ch;
        while (isSpace(ch = nextChar()))
            ;

        do {
            bldr.append((char) ch);
        } while (!isSpace(ch = nextChar()));

        return bldr.toString();
    }

    public int nextInt() throws IOException {
        // considering ASCII files--> 8 bit chars, unicode files has 16 bit chars (byte1
        // then byte2)
        int result = 0;
        int sign = 1;
        int ch;
        while (isSpace(ch = nextChar()))
            ;

        if (ch == '-') {
            sign = -1;
            ch = nextChar();
        }

        do {
            if (ch < '0' || ch > '9')
                throw new NumberFormatException("Found '" + ch + "' while parsing for int.");

            result *= 10;
            result += ch - '0';
        } while (!isSpace(ch = nextChar()));

        return sign * result;
    }

    public long nextLong() throws IOException {
        // considering ASCII files--> 8 bit chars, unicode files has 16 bit chars (byte1
        // then byte2)
        long result = 0;
        int sign = 1;
        int ch;
        while (isSpace(ch = nextChar()))
            ;

        if (ch == '-') {
            sign = -1;
            ch = nextChar();
        }

        do {
            if (ch < '0' || ch > '9')
                throw new NumberFormatException("Found '" + ch + "' while parsing for int.");

            result *= 10;
            result += ch - '0';
        } while (!isSpace(ch = nextChar()));

        return sign * result;
    }

    private boolean isSpace(int ch) {
        return ch == '\n' || ch == ' ' || ch == '\t' || ch == '\r' || ch == -1;
    }

    public void close() throws IOException {
        is.close();
    }
}
