import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
//stable sort
class FinalStanding {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    static void stableSelectionSort(int[] a, int[] b, int n) {
        for (int i = 0; i < n - 1; i++) {
            int max = i;
            for (int j = i + 1; j < n; j++){
                if (a[max] < a[j]){
                    max = j;
                }
            }
            int key = a[max];
            int idKey = b[max];
            while (max > i) {
                a[max] = a[max - 1];
                b[max] = b[max - 1];
                max--;
            }
            a[i] = key;
            b[i] = idKey;
        }
    }

    static void printArray(int[] a, int[] b, int n) {
        for (int i = 0; i <n; i++){
            System.out.print(b[i] + " ");
            System.out.println(a[i]);
        }
    }

    public static void main(String[] args) throws NumberFormatException, IOException 
    { 
        int number = Integer.parseInt(br.readLine());
        int[] sort = new int[number];
        int[] id = new int[number];
        for (int i = 0; i < number; i++) {
            String string = br.readLine();
            String[] stri =  string.split(" ");
            sort[i] = Integer.parseInt(stri[1]);
            id[i] = Integer.parseInt(stri[0]);
        }
        stableSelectionSort(sort, id, sort.length); 
        printArray(sort,id, sort.length); 
    } 
} 