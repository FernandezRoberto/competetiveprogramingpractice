import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class FiveVariables {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        for (int i = 1; i <= 5; i++) {
            if(st.nextToken().equals("0")){
                System.out.println(i);
                break;
            }
        }
    }
}
