import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class FoodAnimals {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int a = Integer.parseInt(st.nextToken());
            int b = Integer.parseInt(st.nextToken());
            int c = Integer.parseInt(st.nextToken());
            int x = Integer.parseInt(st.nextToken());
            int y = Integer.parseInt(st.nextToken());
            if (x == 0 && y==0){
                System.out.println("Yes");
            } else {
                if(x < a){
                    x = 0;
                }else {
                    x -= a;
                }
                if(y < b){
                    y = 0;
                }else {
                    y -= b;
                }


                if(x+y > c){
                    System.out.println("No");
                } else {
                    System.out.println("Yes");
                }
            }
        }
    }
}
