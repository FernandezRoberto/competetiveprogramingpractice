import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class ForbiddenList {
    public static int[] list = new int[100];

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int n1 = Integer.parseInt(st.nextToken());
        int n2 = Integer.parseInt(st.nextToken());
        StringTokenizer st1 = new StringTokenizer(br.readLine());
        if(n2 == 0){
            System.out.println(n1);
            System.exit(0);
        }
        for (int i = 0; i < n2; i++) {
            int num = Integer.parseInt(st1.nextToken());
            list[num - 1] = 1;
        }
        int cont = 1;
        boolean flag = true;
        if(list[n1 - 1] == 0){
            System.out.println(n1);
        }
        while(flag){
            if(n1 - cont >= 0){
                if(list[(n1 - cont)-1] == 0){
                    System.out.println(n1-cont);
                    System.exit(0);
                }
            }
            if (n1 + cont <=100){
                if(list[(n1+cont)-1] == 0){
                    System.out.println(n1+cont);
                    System.exit(0);
                }
            }
            cont++;
        }
    }
}
