import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Gitmv {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str1 = br.readLine();
        String str2 = br.readLine();
        List<String> list1 = new ArrayList<>(Arrays.asList(str1.split("/")));
        List<String> list2 = new ArrayList<>(Arrays.asList(str2.split("/")));
        String resp = "";
        String end = "";
        for (int i = 0; i < list1.size(); i++) {
            if(list1.size()>0 && list2.size()>0) {
                if (list1.get(i).equals(list2.get(i))) {
                    resp = resp + list1.get(i) + "/";
                    list1.remove(i);
                    list2.remove(i);
                    i--;
                } else {
                    resp = resp + "{";
                    break;
                }
            }
        }
        if(!resp.contains("{")){
            resp = resp + "{";
        }
        Collections.reverse(list1);
        Collections.reverse(list2);
        for (int i = 0; i < list1.size(); i++) {
            if(list1.size()>0 && list2.size()>0) {
                if (list1.get(i).equals(list2.get(i))) {
                    end = "/" + list1.get(i) + end;
                    list1.remove(i);
                    list2.remove(i);
                    i--;
                } else {
                    end = "}" + end;
                    break;
                }
            }
        }
        if(!end.contains("}")){
            end = "}" + end;
        }
        StringBuilder middlep1 = new StringBuilder();
        for (String value : list1) {
            middlep1.insert(0, value + "/");
        }
        StringBuilder middlep2 = new StringBuilder();
        for (String s : list2) {
            middlep2.insert(0, s + "/");
        }
        if (middlep1.length() > 0){
            middlep1 = new StringBuilder(middlep1.substring(0, middlep1.length() - 1));
        }
        if (middlep2.length() > 0){
            middlep2 = new StringBuilder(middlep2.substring(0, middlep2.length() - 1));
        }
        resp = resp + middlep1 + " => " + middlep2 + end;
        System.out.println(resp);
    }
}