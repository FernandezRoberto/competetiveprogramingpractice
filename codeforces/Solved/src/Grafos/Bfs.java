package Grafos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;

public class Bfs {
    public static List<Integer> listaAdj[];

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        for (int k = 0; k < n; k++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int vertices = Integer.parseInt(st.nextToken());
            int conexiones = Integer.parseInt(st.nextToken());

            listaAdj = new List[vertices];
            for (int i = 0; i < vertices; ++i) {
                listaAdj[i] = new LinkedList();
            }
            for (int i = 0; i < conexiones; ++i) {
                StringTokenizer st1 = new StringTokenizer(br.readLine());
                int primero = Integer.parseInt(st1.nextToken()) - 1;
                int segundo = Integer.parseInt(st1.nextToken()) - 1;
                addEdge(primero, segundo);
                addEdge(segundo, primero);
            }
            BFS(0, vertices);
        }

    }

    public static void BFS(int inicio, int vertices) {
        boolean visited[] = new boolean[vertices];
        Queue<Integer> queue = new LinkedList<Integer>();
        visited[inicio] = true;
        int[] level = new int[vertices];
        queue.add(inicio);
        while (!queue.isEmpty()) {
            inicio = queue.poll();
            for (int e : listaAdj[inicio]) {
                if (!visited[e]) {
                    visited[e] = true;
                    level[e] = level[e] + level[inicio] + 1;
                    queue.add(e);
                }
                if (e == vertices - 1) {
                    queue.clear();
                    break;
                }
            }

        }
        System.out.println(level[vertices - 1]);
    }

    public static void addEdge(int vertice, int valor) {
        listaAdj[vertice].add(valor);
    }
}
