package Grafos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Graph {
    private int adjMatrix[][];

    public Graph(int rows, int columns) {
        adjMatrix = new int[rows][columns];
    }

    public void addEdge(int i, int j, int number) {
        adjMatrix[i][j] = number;
    }

    public String toString(int rows, int columns) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                s.append(adjMatrix[i][j] + " ");
            }
            s.append("\n");
        }
        return s.toString();
    }

    public static void main(String args[]) throws NumberFormatException, IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int rows = Integer.parseInt(st.nextToken());
        int columns = Integer.parseInt(st.nextToken());
        Graph g = new Graph(rows, columns);
        int x = Integer.parseInt(st.nextToken());
        int y = Integer.parseInt(st.nextToken());
        int cont = 0;
        g.addEdge(x, y, cont);
        int pasos = 1;
        for (int i = 0; i < (rows > columns ? rows : columns) - 1; i++) {
            cont++;
            x--;
            y--;
            pasos = pasos + 2;
            for (int j = 0; j < pasos; j++) {
                if (x >= 0 && x < rows && y >= 0 && y + j < columns) {
                    g.addEdge(x, y + j, cont);
                } 
            }
            for (int j = 0; j < pasos; j++) {
                if(x<0 && j<rows && y<0 && y+(pasos-1) < columns && j+x >= 0){
                    g.addEdge(j+x, y+(pasos-1), cont);
                }
            }
            for (int j = 0; j < pasos; j++) {
                if (x >= 0 && x + (pasos - 1) < rows && y >= 0 && y + j < columns) {
                    g.addEdge(x + (pasos - 1), y + j, cont);
                } 
            }
            for (int j = 0; j < pasos; j++) {
                if (x >= 0 && x + j < rows && y >= 0 && y < columns) {
                    g.addEdge(x + j, y, cont);
                } 
            }
        }

        System.out.print(g.toString(rows, columns));
    }
}