package Grafos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class monks {
    private int adjMatrix[][];

    public monks(int rows, int columns) {
        adjMatrix = new int[rows][columns];
    }

    public void addEdge(int i, int j, int number) {
        adjMatrix[i][j] = number;
    }

    public String toString(int rows, int columns) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                s.append(adjMatrix[i][j] + " ");
            }
            s.append("\n");
        }
        return s.toString();
    }

    public static void main(String args[]) throws NumberFormatException, IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int rows = Integer.parseInt(st.nextToken());
        int columns = Integer.parseInt(st.nextToken());
        Graph g = new Graph(rows, columns);
        int x = Integer.parseInt(st.nextToken());
        int y = Integer.parseInt(st.nextToken());
        int cont = 0;
        g.addEdge(x, y, cont);
        int pasos = 1;
        for (int i = 0; i < (rows > columns ? rows : columns) - 1; i++) {
            cont++;
            x--;
            y--;
            int auxX=x;
            int auxY=y;
            pasos = pasos + 2;
            for (int j = 0; j < pasos-1; j++) {
                if (auxX >= 0 && auxX < rows && auxY >= 0 && auxY < columns) {
                    g.addEdge(auxX, auxY, cont);
                } 
                auxY++;
            }
            for (int j = 0; j < pasos-1; j++) {
                
                if(auxX >= 0 && auxX < rows && auxY >= 0 && auxY < columns){
                    g.addEdge(auxX, auxY, cont);
                }
                auxX++;
            }
            for (int j = 0; j < pasos-1; j++) {
                
                if(auxX >= 0 && auxX < rows && auxY >= 0 && auxY < columns){
                    g.addEdge(auxX, auxY, cont);
                }
                auxY--;
            }
            for (int j = 0; j < pasos-1; j++) {
                
                if(auxX >= 0 && auxX < rows && auxY >= 0 && auxY < columns){
                    g.addEdge(auxX, auxY, cont);
                }
                auxX--;
            }
        }

        System.out.print(g.toString(rows, columns));
    }
}