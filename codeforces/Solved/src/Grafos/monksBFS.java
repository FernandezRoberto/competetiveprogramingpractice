package Grafos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class monksBFS {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String args[]) throws IOException {
        StringTokenizer st = new StringTokenizer(br.readLine());
        int rows = Integer.parseInt(st.nextToken());
        int columns = Integer.parseInt(st.nextToken());
        int x = Integer.parseInt(st.nextToken());
        int y = Integer.parseInt(st.nextToken());
        int adjMatrix[][] = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                adjMatrix[i][j] = -1;
            }
        }
        adjMatrix[x][y] = 0;
        Queue<Integer> xAxis = new LinkedList<>();
        Queue<Integer> yAxis = new LinkedList<>();
        xAxis.add(x);
        yAxis.add(y);
        while (xAxis.size() > 0) {
            x = xAxis.remove();
            y = yAxis.remove();
            if (x + 1 < rows) {
                if (adjMatrix[x + 1][y] == -1) {
                    adjMatrix[x + 1][y] = adjMatrix[x][y] + 1;
                    xAxis.add(x + 1);
                    yAxis.add(y);
                }
                if (y + 1 < columns) {
                    if (adjMatrix[x + 1][y + 1] == -1) {
                        adjMatrix[x + 1][y + 1] = adjMatrix[x][y] + 1;
                        xAxis.add(x + 1);
                        yAxis.add(y + 1);
                    }
                }
                if (y - 1 >= 0) {
                    if (adjMatrix[x + 1][y - 1] == -1) {
                        adjMatrix[x + 1][y - 1] = adjMatrix[x][y] + 1;
                        xAxis.add(x + 1);
                        yAxis.add(y - 1);
                    }
                }
            }
            if (x - 1 >= 0) {
                if (adjMatrix[x - 1][y] == -1) {
                    adjMatrix[x - 1][y] = adjMatrix[x][y] + 1;
                    xAxis.add(x - 1);
                    yAxis.add(y);
                }
                if (y + 1 < columns) {
                    if (adjMatrix[x - 1][y + 1] == -1) {
                        adjMatrix[x - 1][y + 1] = adjMatrix[x][y] + 1;
                        xAxis.add(x - 1);
                        yAxis.add(y + 1);
                    }
                }
                if (y - 1 >= 0) {
                    if (adjMatrix[x - 1][y - 1] == -1) {
                        adjMatrix[x - 1][y - 1] = adjMatrix[x][y] + 1;
                        xAxis.add(x - 1);
                        yAxis.add(y - 1);
                    }
                }
            }
            if (y + 1 < columns) {
                if (adjMatrix[x][y + 1] == -1) {
                    adjMatrix[x][y + 1] = adjMatrix[x][y] + 1;
                    xAxis.add(x);
                    yAxis.add(y + 1);
                }
            }
            if (y - 1 >= 0) {
                if (adjMatrix[x][y - 1] == -1) {
                    adjMatrix[x][y - 1] = adjMatrix[x][y] + 1;
                    xAxis.add(x);
                    yAxis.add(y - 1);
                }
            }
        }
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++)
                System.out.print(adjMatrix[i][j] + " ");
            System.out.println();
        }
    }
}
