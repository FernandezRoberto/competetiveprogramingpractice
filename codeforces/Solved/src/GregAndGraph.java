import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class GregAndGraph {

    public static int vertices;
    public static long[][] mat;
    public static int[] list;
    public static long[] resp;

    public static void main(String[]args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        vertices = Integer.parseInt(br.readLine());
        mat = new long[vertices][vertices];
        for (int i = 0; i < vertices; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            for (int j = 0; j < vertices; j++) {
                mat[i][j] = Long.parseLong(st.nextToken());
            }
        }
        list = new int[vertices];
        StringTokenizer st = new StringTokenizer(br.readLine());
        for (int i = 0; i < vertices; i++) {
            list[vertices-i-1] = Integer.parseInt(st.nextToken())-1;
        }
        solve();

    }

    private static void solve() {
        resp = new long[vertices];
        floydWarshall();
        for (int i = 0; i < vertices; i++) {
            System.out.print(resp[vertices - i -1] + " ");
        }
    }

    private static void floydWarshall() {
        int aux = 0;
        for (int i = 0; i < list.length; i++) {
            int ii = list[i];
            long res = 0;
            for (int j = 0; j < vertices; j++) {
                for (int k = 0; k < vertices; k++) {
                    if (mat[j][ii] + mat[ii][k] < mat[j][k]){
                        mat[j][k] = mat[j][ii] + mat[ii][k];
                    }
                }
            }
            for (int j = 0; j <= aux ; j++) {
                for (int k = 0; k <= aux; k++) {
                    res = res + mat[list[j]][list[k]];
                }
            }
            resp[aux]=res;
            aux++;
        }
    }
}
