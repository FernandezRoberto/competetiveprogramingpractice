import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class HandlingBlocks {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int num = Integer.parseInt(st.nextToken());
        int colors = Integer.parseInt(st.nextToken());
        ArrayList<Integer> ar1 = new ArrayList<>();
        ArrayList<Integer> ar2 = new ArrayList<>();
        ArrayList<Integer> aux = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            StringTokenizer st1 = new StringTokenizer(br.readLine());
            ar1.add(Integer.parseInt(st1.nextToken()));
            ar2.add(Integer.parseInt(st1.nextToken()));
            aux.add(i+1);
        }
        int[] a = solve(ar1, ar2, colors);
        int[] b = solve(aux, ar2, colors);
        if (Arrays.equals(a,b)){
            System.out.println("Y");
        } else {
            System.out.println("N");
        }
    }

    private static int[] solve(ArrayList<Integer> ar1, ArrayList<Integer> ar2, int colors) {
        int[] ans = new int[colors+1];
        for (int i = 0; i < ans.length; i++) {
            ans[i] = 0;
        }
        for (int i = 0; i < ar1.size(); i++) {
            ans[ar2.get(i)] = ans[ar2.get(i)] + ar1.get(i);
        }
        return ans;
    }
}
