import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Hangover {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        boolean flag = true;
        while (flag){
            double cases = Double.parseDouble(br.readLine());
            if (cases == 0.00){
                flag = false;
            } else {
                System.out.println(solve(cases) + " card(s)");
            }
        }
    }

    private static int solve(double a) {
        int k = 1;
        double sum = 0.0;
        while ((sum-1) <= a) {
            sum += 1.0 / k;
            k++;
        }
        return k - 2;
    }
}
