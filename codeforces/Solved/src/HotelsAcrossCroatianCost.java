import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class HotelsAcrossCroatianCost {
    public static int[] list;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int nums = Integer.parseInt(st.nextToken());
        int max = Integer.parseInt(st.nextToken());
        StringTokenizer st1 = new StringTokenizer(br.readLine());
        list = new int[nums];
        for (int i = 0; i < nums; i++) {
            list[i] = Integer.parseInt(st1.nextToken());
        }
        int start = 0;
        int end = 0;
        int sum = 0;
        int ans = 0;
        for (int i = 0; i < nums; i++) {
            if(list[end] + sum > max){
                sum -= list[start];
                start++;
                i--;
                //sum += list[end];
            } else {
                sum += list[end];
                end++;
            }
            if (sum > ans){
                ans = sum;
            }
        }
        System.out.println(ans);
    }
}
