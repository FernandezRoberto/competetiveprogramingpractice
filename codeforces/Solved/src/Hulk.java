import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Hulk {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        String str = "";
        for (int i = 0; i < cases; i++) {
            if (i%2==0){
                str = str + "I hate that ";
            } else {
                str = str + "I love that ";
            }
        }
        System.out.println(str.substring(0, str.length()-5) + "it");
    }
}
