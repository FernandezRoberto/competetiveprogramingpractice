import java.util.ArrayList;
import java.util.Scanner;

public class IQTest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int number;
        number = scan.nextInt();
        int[] list = new int[number];
        for (int i = 0; i < list.length; i++) {
            list[i] = scan.nextInt();
        }
        System.out.println(test(list));
        scan.close();
    }

    static int test(int[] list) {
        ArrayList<Integer> even = new ArrayList<>();
        ArrayList<Integer> odd = new ArrayList<>();
        for (int i = 0; i < list.length; i++) {
            if (list[i] % 2 == 0) {
                even.add(i);
            }  else {
                odd.add(i);
            }
        }
        if(even.size() == 1) {
            return even.get(0) + 1;
        } else {
            return odd.get(0) + 1;
        }
    }
}
