import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.StringTokenizer;

public class IgorMuseum {

    public static char[][] mat;
    public static HashMap<Character, Integer> map = new HashMap<>();
    public static int[][] limits = {{0, -1},{0, 1},{-1, 0},{1, 0}};
    public static int res;
    public static char c = 'a';

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int n = Integer.parseInt(st.nextToken());
        int m = Integer.parseInt(st.nextToken());
        int k = Integer.parseInt(st.nextToken());
        mat = new char[n+1][m+1];
        for (int i = 1; i <= n; i++) {
            st = new StringTokenizer(br.readLine());
            String str = st.nextToken();
            for (int j = 1; j <= m; j++) {
                mat[i][j] = str.charAt(j-1);
            }
        }
        for (int i = 0; i < k; i++) {
            st = new StringTokenizer(br.readLine());
            int x = Integer.parseInt(st.nextToken());
            int y = Integer.parseInt(st.nextToken());
            res = 0;
            if (mat[x][y] == '.'){
                dfs(x,y);
                map.put(c, res);
                c++;
                System.out.println(res);
            } else {
                System.out.println(map.get(mat[x][y]));
            }
        }
    }

    private static void dfs(int i, int j) {
        mat[i][j] = c;
        for (int k = 0; k < 4; k++) {
            int ii = i + limits[k][0];
            int jj = j + limits[k][1];
            if (mat[ii][jj] == '.') {
                dfs(ii, jj);
            } else if (mat[ii][jj] == '*') {
                res++;
            }
        }
    }

}
