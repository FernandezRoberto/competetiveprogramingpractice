import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.StringTokenizer;

public class IntegerDiversity {
    public static LinkedHashSet<Integer> set = new LinkedHashSet<>();
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            int num = Integer.parseInt(br.readLine());
            StringTokenizer st = new StringTokenizer(br.readLine());
            for (int j = 0; j < num; j++) {
                int n = Integer.parseInt(st.nextToken());
                if(set.contains(n)){
                    if(set.contains(n * -1)){
                    } else {
                        set.add(n * -1);
                    }
                } else {
                    set.add(n);
                }
            }
            System.out.println(set.size());
            set.clear();
        }
    }
}
