import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Ivan {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int tests = Integer.parseInt(br.readLine());
        for (int i = 0; i < tests; i++) {
            int max = Integer.MIN_VALUE;
            StringTokenizer st = new StringTokenizer(br.readLine());
            int num = Integer.parseInt(st.nextToken());
            int money = Integer.parseInt(st.nextToken());
            for (int j = 0; j < num; j++) {
                StringTokenizer st1 = new StringTokenizer(br.readLine());
                int cost = Integer.parseInt(st1.nextToken());
                int games = Integer.parseInt(st1.nextToken());
                if(money >= cost){
                    if(max < games){
                        max = games;
                    }
                }
            }
            System.out.println(max);
        }

    }
}
