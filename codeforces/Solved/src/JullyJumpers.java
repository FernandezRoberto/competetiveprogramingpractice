import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class JullyJumpers {
    
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = br.readLine()) != null) {
            line = line.trim();
            String[] numbersString = line.split("\\s+");
            int[] numbers = new int[numbersString.length];
            for (int i = 0; i < numbers.length; i++) {
                numbers[i] = Integer.parseInt(numbersString[i]);
            }
            System.out.println(isJumper(numbers) ? "Jolly" : "Not jolly");
        }
    }

    public static boolean isJumper(int[] array) {
        int[] result = new int[array[0]];
        for (int i = 0; i < array[0]; i++) {
            result[i] = i;
        }
        for (int i = 1; i < array.length - 1; i++) {
            int diff = Math.abs(array[i] - array[i + 1]);
            if(result.length > diff && result[diff] != 0) {
                array[0]--;
                result[diff] = 0;
            }
        }
        return array[0] == 1;
    }
    
}