import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Kathmandu {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int minutes = Integer.parseInt(st.nextToken());
        int duration = Integer.parseInt(st.nextToken());
        int meals = Integer.parseInt(st.nextToken());
        int start = 0;
        boolean flag = true;
        for (int i = 0; i < meals; i++) {
            int time = Integer.parseInt(br.readLine());
            if(time-start >= minutes){
                System.out.println("Y");
                flag = false;
                break;
            } else {
                start = time;
            }
        }
        if (flag){
            if (duration-start >= minutes){
                System.out.println("Y");
            } else {
                System.out.println("N");
            }
        }

    }
}
