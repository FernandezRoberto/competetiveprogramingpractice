import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.TreeSet;

public class Kiara {
    public static String[] words;
    public static TreeSet<Character> list = new TreeSet<>();
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine());
        words = new String[num];
        for (int i = 0; i < num; i++) {
            String str = br.readLine();
            words[i] = str;
            list.add(str.charAt(0));
        }
        boolean flag = true;
        for (String str: words) {
            flag = true;
            for (int i = 1; i < str.length(); i++) {
                if (!list.contains(str.charAt(i))){
                    flag = false;
                    break;
                }
            }
            if (flag){
                System.out.println("Y");
                System.exit(0);
            }
        }
        if (!flag){
            System.out.println("N");
        }
    }
}
