import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Kidnapped {
    public static char[][] mat = new char[500][500];
    public static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    public static boolean[][] visited = new boolean[500][500];
    public static int sx, sy, ex, ey;
    public static Cube[] d = {new Cube(0,1), new Cube(1,0), new Cube(-1,0), new Cube(0,-1) };
    public static Map<Cube, Character> map = Map.of(new Cube(0,1),'U', new Cube(1,0),'D',
            new Cube(-1
                    ,0),'R',
            new Cube(0,-1),'L');
    public static int row;
    public static int col;
    public static class Cube{
        int x;
        int y;
        Cube prev;
        Cube(int x, int y){
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Cube cube = (Cube) o;
            return x == cube.x && y == cube.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }

    public static void main(String[] args) throws IOException {
        int tests = Integer.parseInt(br.readLine());
        for (int i = 0; i < tests; i++) {
            solve();
        }
    }

    private static void solve() throws IOException {
        StringTokenizer st = new StringTokenizer(br.readLine());
        row = Integer.parseInt(st.nextToken());
        col = Integer.parseInt(st.nextToken());
        for (int i = 0; i < row; i++) {
            Arrays.fill(visited[i],false);
        }
        for (int i = 0; i < row; i++) {
            String s = br.readLine();
            for (int j = 0; j < col; j++) {
                mat[i][j] = s.charAt(j);
                if(mat[i][j] == 'S'){
                    sx = i;
                    sy = j;
                }
                if (mat[i][j] == 'X'){
                    ex = i;
                    ey = j;
                }
            }
        }
        bfs();
    }

    private static void bfs() {
        Queue<Cube> x = new ArrayDeque<>();

        x.add(new Cube(sx,sy));
        visited[sx][sy] = true;

        while (x.size() > 0){
            Cube c = x.poll();
            if (c.x == ex && c.y == ey){
                Stack<Character> stack = new Stack<>();
                while (c.prev != null){
                    stack.push(map.get(new Cube(c.x - c.prev.x, c.y - c.prev.y)));
                    c = c.prev;
                }
                for (Character car : stack) {
                    System.out.print(car);
                }
                System.out.println();
                break;
            }
            for (Cube ds: d) {
                Cube a = new Cube(c.x + ds.x, c.y + ds.y);
                a.prev = c;
                if(a.x < 0 || a.y < 0 || a.x >= row || a.y >= col || visited[a.x][a.y] || mat[a.x][a.y] == '#') continue;
                x.add(a);
                visited[a.x][a.y] = true;
            }
        }
        System.out.println("No exit");
    }


}