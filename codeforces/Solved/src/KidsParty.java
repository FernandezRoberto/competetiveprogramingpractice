import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class KidsParty {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine());
        for (int i = 0; i < num; i++) {
            BigInteger n = new BigInteger(br.readLine());
            System.out.println(solve(n));
        }
    }

    private static String solve(BigInteger n) {
        String ans = "";
        boolean two = false;
        boolean three = false;
        if (n.mod(BigInteger.TWO).equals(BigInteger.ZERO)){
            ans = ans + "2";
            two = true;
        }
        if (checkDivisibleBy3(n.toString())){
            ans = ans + " 3";
            three = true;
        }
        if (checkDivisibleBy4(n.toString())) {
            ans = ans + " 4";
        }
        if (checkDivisibleBy5(n.toString())) {
            ans = ans + " 5";
        }
        if(two && three) {
            ans = ans + " 6";
        }
        if(ans.isEmpty()){
            ans = "-1";
        }
        return ans.strip();
    }

    static boolean checkDivisibleBy3(String str) {
        int n = str.length();
        int digitSum = 0;
        for (int i=0; i<n; i++) {
            digitSum += (str.charAt(i)-'0');
        }
        return (digitSum % 3 == 0);
    }

    static boolean checkDivisibleBy4(String str) {
        int n = str.length();
        if (n == 0) {
            return false;
        }
        if (n == 1) {
            return ((str.charAt(0) - '0') % 4 == 0);
        }
        int last = str.charAt(n - 1) - '0';
        int second_last = str.charAt(n - 2) - '0';
        return ((second_last * 10 + last) % 4 == 0);
    }

    static boolean checkDivisibleBy5(String num) {
        int sz = num.length();
        int i = Integer.parseInt(num.substring(sz - 1));
        if (i == 5) return true;
        return i == 0;
    }
}
