import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class KitchenWaste {
    static class SegmentTree {
        int[] st;
        SegmentTree(int[] arr, int n) {
            int x = (int) (Math.ceil(Math.log(n) / Math.log(2)));
            int max_size = 2 * (int) Math.pow(2, x) - 1;
            st = new int[max_size];
            constructSTUtil(arr, 0, n - 1, 0);
        }
        int getMid(int s, int e) {
            return s + (e - s) / 2;
        }

        int getSumUtil(int ss, int se, int qs, int qe, int si) {
            if (qs <= ss && qe >= se)
                return st[si];
            if (se < qs || ss > qe)
                return 0;
            int mid = getMid(ss, se);
            return getSumUtil(ss, mid, qs, qe, 2 * si + 1) +
                    getSumUtil(mid + 1, se, qs, qe, 2 * si + 2);
        }

        int getSum(int n, int qs, int qe) {
            return getSumUtil(0, n - 1, qs, qe, 0);
        }

        int constructSTUtil(int[] arr, int ss, int se, int si) {
            if (ss == se) {
                st[si] = arr[ss];
                return arr[ss];
            }
            int mid = getMid(ss, se);
            st[si] = constructSTUtil(arr, ss, mid, si * 2 + 1) +
                    constructSTUtil(arr, mid + 1, se, si * 2 + 2);
            return st[si];
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int num = Integer.parseInt(st.nextToken());
        int num2 = Integer.parseInt(st.nextToken());
        int[] list1 = new int[num];
        int[] list2 = new int[num2];
        StringTokenizer st1 = new StringTokenizer(br.readLine());
        StringTokenizer st2 = new StringTokenizer(br.readLine());
        for (int i = 0; i < num; i++) {
           list1[i] = Integer.parseInt(st1.nextToken());
        }
        for (int i = 0; i < num2; i++) {
            list2[i] = Integer.parseInt(st2.nextToken());
        }
        SegmentTree tree1 = new SegmentTree(list1, list1.length);
        SegmentTree tree2 = new SegmentTree(list2, list2.length);
        System.out.println(tree2.getSum(list2.length, 0, list2.length) - tree1.getSum(list1.length,
                0, list1.length));
    }
}
