import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class LongestPath {

    public static Map<Integer, ArrayList<Integer>> adjacentList = new HashMap<>();
    public static Map<Integer, Boolean> visitList = new HashMap<>();
    public static int maxEdges = 0;
    public static int farthestLeaf = 0;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int nodes = Integer.parseInt(br.readLine());
        for (int i = 0; i < nodes-1; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int u = Integer.parseInt(st.nextToken());
            int v = Integer.parseInt(st.nextToken());
            if(!adjacentList.containsKey(u)){
                adjacentList.put(u, new ArrayList<>());
                visitList.put(u, false);
            }
            if (!adjacentList.containsKey(v)){
                adjacentList.put(v, new ArrayList<>());
                visitList.put(v, false);
            }
            adjacentList.get(u).add(v);
            adjacentList.get(v).add(u);
        }
        dfs(2, -1);
        maxEdges = 0;
        for (int i = 1; i <= nodes; i++) {
            visitList.replace(i, false);
        }
        dfs(farthestLeaf, -1);
        System.out.println(maxEdges);

    }

    private static void dfs(int leaf, int count) {
        visitList.put(leaf, true);
        count++;
        if (adjacentList.get(leaf).size() == 1 && visitList.get(adjacentList.get(leaf).get(0))){
            if (count > maxEdges){
                maxEdges = count;
                farthestLeaf = leaf;
            }
        } else {
            for (int i: adjacentList.get(leaf)) {
                if(!visitList.get(i)){
                    dfs(i, count);
                }
            }
        }
    }
}
