import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LuckyNumbers {

    private static final int[] memo = new int [37];
    private static int n;

    static void solve(int n, int sum){
        if(n == LuckyNumbers.n / 2) {
            memo[sum]++;
        } else {
            for(int i = 0; i<10; i++) {
                solve(n+1,sum+i);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        n = Integer.parseInt(br.readLine());
        solve(0,0);
        int answer = 0;
        for(int i = 0; i<=36; i++) {
            answer += memo[i] * memo[i];
        }
        System.out.println(answer);
    }
}