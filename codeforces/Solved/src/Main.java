import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
//        final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        final BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        final Solver solver = new Solver();

        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String word = sc.nextLine();
            String result = solver.solve(word);
            bw.write(result);
            System.out.println();
            bw.flush();
        }
        sc.close();
    }
}

class Solver {

    private final HashMap<Character, Character> reverses;

    public Solver() {
        this.reverses = new HashMap<>();
        reverses.put('A', 'A');
        reverses.put('E', '3');
        reverses.put('H', 'H');
        reverses.put('I', 'I');
        reverses.put('J', 'L');
        reverses.put('L', 'J');
        reverses.put('M', 'M');
        reverses.put('S', '2');
        reverses.put('O', 'O');
        reverses.put('T', 'T');
        reverses.put('U', 'U');
        reverses.put('V', 'V');
        reverses.put('W', 'W');
        reverses.put('X', 'X');
        reverses.put('Y', 'Y');
        reverses.put('Z', '5');
        reverses.put('1', '1');
        reverses.put('2', 'S');
        reverses.put('3', 'E');
        reverses.put('5', 'Z');
    }

    public String solve(String word) {
        boolean mirror = true;
        boolean regular = true;
        char[] chars = word.toCharArray();
        for (int i = 0; i <= chars.length / 2 && (mirror || regular); i++) {
            char forward = chars[i];
            char backward = chars[chars.length - 1 - i];
            if (forward != backward) {
                regular = false;
            }
            if (mirror) {
                char reverse = getReverse(forward);
                if (reverse == '-') {
                    mirror = false;
                }
            }
        }
        if (mirror && regular) {
            return " " + word + " -- is a mirrored palindrome.";
        }
        if (mirror) {
            return " " + word + " -- is a mirrored string.";
        }
        if (regular) {
            return " " + word + " -- is a regular palindrome.";
        }
        return " " + word + " -- is not a palindrome.";
    }

    private char getReverse(char c) {
         return reverses.getOrDefault(c, '-');
    }
}
