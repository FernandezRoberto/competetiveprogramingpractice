import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class MinMaxSwap {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            int number = Integer.parseInt(br.readLine());
            StringTokenizer st1 = new StringTokenizer(br.readLine());
            StringTokenizer st2 = new StringTokenizer(br.readLine());
            int max1 = 0;
            int max2 = 0;
            for (int j = 0; j < number; j++) {
                int n1 = Integer.parseInt(st1.nextToken());
                int n2 = Integer.parseInt(st2.nextToken());
                if(n1<n2){
                    if(n2 > max1){
                        max1 = n2;
                    }
                    if (n1 > max2 ){
                        max2 = n1;
                    }
                } else {
                    if(n1 > max1){
                        max1 = n1;
                    }
                    if (n2 > max2 ){
                        max2 = n2;
                    }
                }
            }
            System.out.println(max1*max2);
        }
    }
}
