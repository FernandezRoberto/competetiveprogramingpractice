import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class MirrorTheString {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            int num = Integer.parseInt(br.readLine());
            String str = br.readLine();
            System.out.println(solve(num, str));
        }
    }

    private static String solve(int num, String str) {
        String ans = "";
        int init = 1;
        int pos = 1;
        for (int i = 0; i < num - 1; i++) {
            for (int j = 0; j < init; j++) {
                String substring = str.substring(i - j, i+1);
                if(substring.equals(str.substring(pos, pos + 1 + j))){
                    StringBuilder sb =new StringBuilder(substring);
                    return substring + sb.reverse();
                }
            }
            if (init + i < num -1) {
                init++;
            } else {
                init--;
            }
            pos++;
        }
        ans = str.charAt(0) + "" + str.charAt(0);
        return ans;
    }
}
