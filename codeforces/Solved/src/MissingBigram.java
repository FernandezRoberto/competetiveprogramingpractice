import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class MissingBigram {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int tests = Integer.parseInt(br.readLine());
        for (int i = 0; i < tests; i++) {
            String resp = "";
            int num = Integer.parseInt(br.readLine());
            StringTokenizer st = new StringTokenizer(br.readLine());
            resp = st.nextToken();
            while(st.hasMoreTokens()){
                String str = st.nextToken();
                if(resp.charAt(resp.length()-1) == str.charAt(0)){
                    resp = resp + str.charAt(1);
                } else {
                    resp = resp + str;
                }
            }
            if(resp.length() < num){
                resp = resp + "a";
            }
            System.out.println(resp);
        }

    }
}
