import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class MultiplicationTwo {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        StringTokenizer st = new StringTokenizer(br.readLine());
        long ans = 1;
        for (int i = 0; i < cases; i++) {

            try {
                long n1 = Long.parseLong(st.nextToken());
                ans = n1 * ans;
            } catch (NumberFormatException e) {
                System.out.println(-1);
                System.exit(0);
            }
        }

        System.out.println((ans > Long.MAX_VALUE) ? -1 : ans);
    }
}
