import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Multiplicative {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine());
        for (int i = 0; i < num; i++) {
            int n = Integer.parseInt(br.readLine());
            System.out.println(solve(n));
        }
    }

    private static int solve(int n) {
        int ans = 0;
        while(n > 9) {
            n = multiplyAllDigits(n);
            ans++;
        }
        return ans;
    }

    private static int multiplyAllDigits(int n) {
        int ans = 1;
        while(n>0){
            ans = (n % 10) * ans;
            n = n / 10;
        }
        return ans;
    }
}
