import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NearlyLuckyNumbers {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        long n = Long.parseLong(br.readLine());
        long cont = 0;
        while(n>0){
            long dig = n%10;
            n = n/10;
            if (dig == 4 || dig == 7) {
                cont++;
            }
        };
        if (cont == 0){
            System.out.println("NO");
        } else {
            while(cont>0){
                if(cont%10 != 4 && cont%10 != 7){
                    System.out.println("NO");
                    break;
                }
                cont = cont/10;
            }
            if (cont==0){
                System.out.println("YES");
            }
        }
    }
}
