import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NextPrime {
    static int next = 0;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine());
        System.out.println(nextPrimeNumber(num));
    }

    static boolean isPrime(int n) {
        for (int x = 2; x <= Math.sqrt(n); x++) {
            if (n % x == 0) {
                return false;
            }
        }
        return true;
    }

    static int nextPrimeNumber(int n) {
        if (isPrime(n)) {
            next = n;
            return next;
        } else {
            next = n + 1;
            nextPrimeNumber(next);
        }
        return next;
    }
}
