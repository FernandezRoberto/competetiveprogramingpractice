import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class OddsEvensIncrement {
    public static int[] list;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            int num = Integer.parseInt(br.readLine());
            StringTokenizer st = new StringTokenizer(br.readLine());
            boolean evenEven = false, evenOdd = false, oddOdd = false, oddEven = false;
            list = new int[num];
            for (int j = 0; j < num; j++) {
                list[j] = Integer.parseInt(st.nextToken());
            }
            for (int j = 0; j < num; j+=2) {
                if(list[j]%2==0){
                    evenEven = true;
                } else {
                    evenOdd = true;
                }
            }
            for (int j = 1; j < num; j+=2) {
                if(list[j]%2==0){
                    oddEven = true;
                } else {
                    oddOdd = true;
                }
            }
            System.out.println((evenEven && evenOdd) || (oddOdd && oddEven) ? "No" : "Yes");
        }
    }
}
