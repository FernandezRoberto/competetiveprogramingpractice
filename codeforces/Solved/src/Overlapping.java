
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Overlapping {
    public static Rectangle[] rectangles;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        rectangles = new Rectangle[cases];
        for (int i = 0; i < cases; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int bl = Integer.parseInt(st.nextToken());
            int tl = Integer.parseInt(st.nextToken());
            int brr = Integer.parseInt(st.nextToken());
            int tr = Integer.parseInt(st.nextToken());

            Rectangle rectangle = new Rectangle(new Coordenada(bl, tl), new Coordenada(bl, tr),
                    new Coordenada(brr, tl), new Coordenada(brr, tr));
            rectangles[i] = rectangle;
        }
        for (int i = 0; i < rectangles.length; i++) {
            for (int j = i+1; j < rectangles.length - 1; j++) {
                if(isOverlapping(rectangles[i], rectangles[j])){
                    System.out.println(1);
                    System.exit(0);
                }
            }
        }
        System.out.println(0);
    }

    public static boolean isOverlapping(Rectangle rectangle1, Rectangle rectangle2) {
        if (rectangle1.getTopLeft().getX() > rectangle2.getBottomRight().getX()
                || rectangle1.getBottomRight().getX() < rectangle2.getTopLeft().getX()
                || rectangle1.getTopLeft().getY() > rectangle2.getBottomRight().getY()
                || rectangle1.getBottomRight().getY() < rectangle2.getTopLeft().getY() ) {
            return false;
        } else {
            return true;
        }

    }

    private static class Rectangle {
        Coordenada topRight;
        Coordenada topLeft;
        Coordenada bottomRight;
        Coordenada bottomLeft;

        public Rectangle(Coordenada bottomLeft, Coordenada topLeft, Coordenada bottomRight,
                         Coordenada topRight) {
            this.topRight = topRight;
            this.topLeft = topLeft;
            this.bottomRight = bottomRight;
            this.bottomLeft = bottomLeft;
        }

        public Coordenada getTopRight() {
            return topRight;
        }

        public Coordenada getTopLeft() {
            return topLeft;
        }

        public Coordenada getBottomRight() {
            return bottomRight;
        }

        public Coordenada getBottomLeft() {
            return bottomLeft;
        }

    }

    private static class Coordenada {
        int x;
        int y;

        public Coordenada(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
}
