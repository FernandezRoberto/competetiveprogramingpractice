import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PRA {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            long wheelsMax = Long.parseLong(br.readLine());
            long wheelsMin = wheelsMax;
            long axlMin = 0;
            long axlMax = 0;
            boolean maxFlag = true;
            boolean minFlag = true;
            while(maxFlag || minFlag){
                if (minFlag){
                    if (wheelsMin >= 4){
                        if (wheelsMin % 6 != 0){
                            wheelsMin -= 4;
                            axlMin++;
                            if (wheelsMin == 0){
                                minFlag = false;
                            }
                        } else {
                            axlMin += wheelsMin/6;
                            wheelsMin = wheelsMin/6;
                            minFlag = false;
                        }
                    } else {
                        minFlag = false;
                        axlMin = -1;
                    }
                }
                if (maxFlag){
                    if (wheelsMax >= 6) {
                        if (wheelsMax % 4 != 0) {
                            wheelsMax -= 6;
                            axlMax++;
                            if (wheelsMax == 0){
                                maxFlag = false;
                            }
                        } else {
                            axlMax += wheelsMax / 4;
                            wheelsMax = wheelsMax / 4;
                            maxFlag = false;
                        }
                    } else {
                        maxFlag = false;
                        axlMax = -1;
                    }
                }
            }
            if (axlMax== -1 && axlMin == -1){
                System.out.println(-1);
            } else {
                if (axlMax == -1){
                    System.out.println(axlMin + " " + axlMin);
                } else if(axlMin == -2){
                    System.out.println(axlMax + " " + axlMax);
                } else {
                    System.out.println(axlMin + " " + axlMax);
                }

            }
        }
    }
}
