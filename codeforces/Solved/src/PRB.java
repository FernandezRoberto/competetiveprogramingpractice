import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringTokenizer;
import java.util.Vector;

public class PRB {
    public static int[] list;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int elements = Integer.parseInt(st.nextToken());
        int queries = Integer.parseInt(st.nextToken());
        StringTokenizer st1 = new StringTokenizer(br.readLine());
        list = new int[elements];
        int sum = 0;
        for (int i = 0; i < elements; i++) {
            int num = Integer.parseInt(st1.nextToken());
            list[i] = num;
            sum += num;
        }
        for (int i = 0; i < queries; i++) {
            StringTokenizer st2 = new StringTokenizer(br.readLine());
            int n = Integer.parseInt(st2.nextToken());
            if (n == 1){
                int n1 = Integer.parseInt(st2.nextToken());
                int n2 = Integer.parseInt(st2.nextToken());
                if(list[n1-1] - n2 < 0){
                    sum = sum + (Math.abs(list[n1-1] - n2));
                } else {
                    sum = sum - (Math.abs(list[n1-1] - n2));
                }
                list[n1-1] = n2;
            } else {
                int n1 = Integer.parseInt(st2.nextToken());
                sum = n1 * elements;
                Arrays.setAll(list, index -> n1);
            }
            System.out.println(sum);
        }
    }
    /*public static void bytefill(int[] array, int value) {
        int len = array.length;
        if (len > 0)
            array[0] = value;
        for (int i = 1; i < len; i += i)
            System.arraycopy( array, 0, array, i,
                    ((len - i) < i) ? (len - i) : i);
    }*/
}
