import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class PROBLEMB {
    public static int[] list;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            int num = Integer.parseInt(br.readLine());
            StringTokenizer st = new StringTokenizer(br.readLine());
            list = new int[num];
            for (int j = 0; j < num; j++) {
                list[j] = Integer.parseInt(st.nextToken());
            }
            int shuffle = Integer.  parseInt(br.readLine());
            StringTokenizer st1 = new StringTokenizer(br.readLine());
            for (int j = 0; j < shuffle; j++) {
                int repeat = Integer.parseInt(st1.nextToken());
                int aux = list[0];
                for (int k = 0; k < list.length; k++) {
                    //int aux = list[k];
                    if(k+repeat >= list.length){
                        int aux1 = list[(k+repeat)- list.length];
                        list[(k+repeat)- list.length] = aux;
                        aux = aux1;
                        //list[(k+repeat)- list.length] = aux;
                    } else {
                        int aux1 = list[k+repeat];
                        list[k+repeat] = aux;
                        aux = aux1;
                    }
                }
            }
            System.out.println(list[0]);
        }

    }
}
