import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class PROBLEMC {
    public static int[] list1;
    public static int[] list2;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            int num = Integer.parseInt(br.readLine());
            list1 = new int[num];
            list2 = new int[num];
            StringTokenizer st1 = new StringTokenizer(br.readLine());
            StringTokenizer st2 = new StringTokenizer(br.readLine());
            for (int j = 0; j < num; j++) {
                list1[j] = Integer.parseInt(st1.nextToken());
                list2[j] = Integer.parseInt(st2.nextToken());
            }
            if(sort(list1, list2)){
                System.out.println(pairs.size()/2);
                for (int j = 0; j < pairs.size(); j = j+2) {
                    System.out.println(pairs.get(j) + " " + pairs.get(j+1));
                }
            } else {
                System.out.println(-1);
            }
        }
    }
    public static ArrayList<Integer> pairs = new ArrayList<>();
    public static boolean sort(int list1[], int list2[]) {
        int n = list1.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (list1[j] > list1[j + 1] && list2[j] > list2[j + 1]) {
                    int temp = list1[j];
                    list1[j] = list1[j + 1];
                    list1[j + 1] = temp;
                    temp = list2[j];
                    list2[j] = list2[j + 1];
                    list2[j + 1] = temp;
                    pairs.add(i);
                    pairs.add(j);
                } else if ((list1[j] < list1[j + 1] && list2[j] > list2[j + 1]) || (list1[j] > list1[j + 1] && list2[j] < list2[j + 1])) {
                    return false;
                }
            }
        }
        return true;
    }
}
