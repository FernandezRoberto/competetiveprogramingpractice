import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class PaintTheArray {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int tests = Integer.parseInt(br.readLine());
        for (int i = 0; i < tests; i++) {
            int num = Integer.parseInt(br.readLine());
            ArrayList<Long> list = new ArrayList<>();
            StringTokenizer st = new StringTokenizer(br.readLine());
            for (int j = 0; j < num; j++) {
                list.add(Long.parseLong(st.nextToken()));
            }

            boolean flag = false;
            for (int j = 0; j < num; j++) {
                int cont = 0;
                int old = 0;
                for (int k = 0; k < num; k++) {
                    if((long)list.get(k) % (long)list.get(j) == 0){
                        cont = 1;
                    } else {
                        cont = -1;
                    }
                    if(cont == old){
                        break;
                    } else {
                        old = cont;
                    }
                    if(k == num-1){
                        System.out.println((long)list.get(j));
                        j=num;
                        flag = true;
                        break;
                    }
                }
            }
            if (!flag){
                System.out.println((long)0);
            }
        }

    }
}
