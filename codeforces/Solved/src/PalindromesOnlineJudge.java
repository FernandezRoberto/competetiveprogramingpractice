import java.util.Scanner;

public class PalindromesOnlineJudge {
    public static void main(String[] args) {
        //final Solver solver = new Solver();

        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String word = sc.nextLine();
            System.out.println(palindrome(word));
            System.out.println();
        }
        sc.close();
    }

    static String palindrome(String str) {
        boolean isPalindrome = verifiPalindrome(str);
        boolean isMirrored = verifiMirrored(str);
        if(isPalindrome && isMirrored) {
            return str + " -- is a mirrored palindrome.";
        } else if (isPalindrome && isMirrored == false) {
            return str + " -- is a regular palindrome.";
        } else if (isPalindrome == false && isMirrored) {
            return str + " -- is a mirrored string.";
        } else {
            return str + " -- is not a palindrome.";
        }
    }

    static boolean verifiPalindrome(String str) {
        for (int i = 0; i < str.length(); i++) {
            if(str.charAt(i) != str.charAt(str.length() - 1 - i)) {
                return false;
            }
        }
        return true;
    }

    static boolean verifiMirrored(String str) {
        boolean flag = false;
        for (int i = 0; i < str.length(); i++) {
            if (!verifiTwoMirrorLetters(str.charAt(i), str.charAt(str.length() - 1 - i))) {
                return false;
            }
            if (verifiTwoMirrorLetters(str.charAt(i), str.charAt(str.length() - 1 - i))) {
                flag = true;
            }       
        }
        if(flag){
            return true;
        } else {
            return false;
        }
        
    }


    private static boolean verifiTwoMirrorLetters(char char1, char char2) {
        if(char1 == 'A' && char2 == 'A' || char1 == 'E' && char2 == '3' || char1 == 'H' && char2 == 'H' 
        || char1 == 'I' && char2 == 'I' || char1 == 'J' && char2 == 'L' || char1 == 'L' && char2 == 'J'
        || char1 == 'M' && char2 == 'M' || char1 == 'O' && char2 == 'O' || char1 == 'S' && char2 == '2'
        || char1 == 'T' && char2 == 'T' || char1 == 'U' && char2 == 'U' || char1 == 'V' && char2 == 'V'
        || char1 == 'W' && char2 == 'W' || char1 == 'X' && char2 == 'X' || char1 == 'Y' && char2 == 'Y'
        || char1 == 'Z' && char2 == '5' || char1 == '1' && char2 == '1' || char1 == '2' && char2 == 'S'
        || char1 == '3' && char2 == 'E' || char1 == '5' && char2 == 'Z' || char1 == '8' && char2 == '8') {
            return true;
        }
        return false;
    }

}
/*if(char1 == 'A' && char2 == 'A' || char1 == 'E' && char2 == '3' || char1 == 'H' && char2 == 'H' 
        || char1 == 'I' && char2 == 'I' || char1 == 'J' && char2 == 'L' || char1 == 'L' && char2 == 'J'
        || char1 == 'M' && char2 == 'M' || char1 == 'O' && char2 == 'O' || char1 == 'S' && char2 == '2'
        || char1 == 'T' && char2 == 'T' || char1 == 'U' && char2 == 'U' || char1 == 'V' && char2 == 'V'
        || char1 == 'W' && char2 == 'W' || char1 == 'X' && char2 == 'X' || char1 == 'Y' && char2 == 'Y'
        || char1 == 'Z' && char2 == '5' || char1 == '1' && char2 == '1' || char1 == '2' && char2 == 'S'
        || char1 == '3' && char2 == 'E' || char1 == '5' && char2 == 'Z' || char1 == '8' && char2 == '8') {
            return true;
        }
        return false;*/