import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PalindromicIndices {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            int num = Integer.parseInt(br.readLine());
            String str = br.readLine();
            int count = num % 2;
            for(int j=(num+1)/2; j<num; j++) {
                if(str.charAt(j) == str.charAt(num/2)) {
                    count += 2;
                } else {
                    break;
                }
            }
            System.out.println(count);
        }

    }
}
