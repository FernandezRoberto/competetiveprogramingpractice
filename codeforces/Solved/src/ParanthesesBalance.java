import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

public class ParanthesesBalance {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws NumberFormatException, IOException {
        int number = Integer.parseInt(br.readLine());
        balance(number);
    }

    static void balance(int number) throws IOException {
        for (int i = 0; i < number; i++) {
            Stack<Character> stack = new Stack<Character>();
            String str = br.readLine();
            for (int j = 0; j < str.length(); j++) {
               if(stack.isEmpty()) {
                   stack.add(str.charAt(j));
               } else if(opposites(str.charAt(j), stack.peek())) {
                   stack.pop();
               } else {
                   stack.add(str.charAt(j));
               }
            }
            if(stack.isEmpty()) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        }
    }

    private static boolean opposites(char first, Character secound) {
        if(first == ')' && secound == '('){
            return true;
        } else if(first == ']' && secound == '[') {
            return true;
        }
        return false;
    }
}
