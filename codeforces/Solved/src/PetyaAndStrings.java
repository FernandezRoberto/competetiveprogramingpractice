import java.util.Scanner;

public class PetyaAndStrings {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str1, str2;
        str1 = scan.next();
        str2 = scan.next();
        System.out.println(petyaStrings(str1, str2));
        scan.close();
    }

    static int petyaStrings(String str1, String str2) {
        str1 = str1.toLowerCase();
        str2 = str2.toLowerCase();
        for (int i = 0; i < str1.length(); i++) {
            if(str1.charAt(i) < str2.charAt(i)) {
                return -1;
            } else if(str1.charAt(i) > str2.charAt(i)) {
                return 1;
            }
        }
        return 0;
    }
}
