import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class PlayWithMath {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int a = Integer.parseInt(st.nextToken());
            int b = Integer.parseInt(st.nextToken());
            solve(a, b);
        }
    }

    public static void solve(int a, int b) {
        int common = gcd(a, b);
        System.out.println(b/common + " " + a/common);
    }

    public static int gcd(int a, int b) {
        while (b != 0) {
            int aux = a % b;
            a = b;
            b = aux;
        }
        return a;
    }
}
