import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Polycarp {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int tests = Integer.parseInt(br.readLine());
        for (int i = 0; i < tests; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int[] ans = new int[3];
            ArrayList<Integer> list = new ArrayList<>();
            ans[0] = Integer.parseInt(st.nextToken());
            ans[1] = Integer.parseInt(st.nextToken());
            for (int j = 0; j < 5; j++) {
                int n = Integer.parseInt(st.nextToken());
                if(n != (ans[0] + ans[1]) || list.contains((n))){
                    ans[2] = n;
                    break;
                } else {
                    list.add(n);
                }
            }
            String resp="";
            for (int j = 0; j < ans.length; j++) {
                resp = resp + ans[j] + " ";
            }
            System.out.println(resp.strip());
        }

    }

}
