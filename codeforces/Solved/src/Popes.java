import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Popes {

    public static int[] list;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        do {
            int years = Integer.parseInt(br.readLine());
            int popes = Integer.parseInt(br.readLine());
            list = new int[popes];
            for (int i = 0; i < popes; i++) {
                list[i] = Integer.parseInt(br.readLine());
            }
            solve(years);
            br.readLine();
            if (!br.ready()){
                System.exit(0);
            }
        } while (true);

    }

    private static void solve(int years) {
        int largestPope = 0;
        int firstPope = 0;
        int lastPope = 0;
        for (int i = 0; i < list.length; i++) {
            //int aux = Arrays.binarySearch(list, i, list.length-1, list[i]+years-1);
            int aux = binarySearch(i, list.length-1, list[i]+years-1);
            if (aux-i+1 > largestPope){
                largestPope = aux-i+1;
                firstPope = list[i];
                lastPope = list[aux];
            }
        }
        System.out.println(largestPope + " " + firstPope + " " + lastPope);
    }

    private static int binarySearch(int from, int to, int value) {
        if(from > to){
            return to;
        }
        int aux = (from + to)/2;
        if (list[aux] == value) {
            int cont = aux;
            for (int i = aux + 1; i < list.length; i++) {
                if (list[i] == value) {
                    cont++;
                }
            }
            return cont;
        } else if(list[aux] > value) {
            return binarySearch(from, aux-1,value);
        } else {
            return binarySearch(aux + 1, to, value);
        }
    }

}
