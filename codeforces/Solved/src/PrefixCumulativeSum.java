import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class PrefixCumulativeSum {
    public static int[] list;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int nums = Integer.parseInt(br.readLine());
        StringTokenizer st1 = new StringTokenizer(br.readLine());
        list = new int[nums];
        int sum = 0;
        for (int i = 0; i < nums; i++) {
            sum += Integer.parseInt(st1.nextToken());
            list[i] = sum;
        }
        int queries = Integer.parseInt(br.readLine());
        for (int i = 0; i < queries; i++) {
            StringTokenizer st2 = new StringTokenizer(br.readLine());
            int start = Integer.parseInt(st2.nextToken());
            int end = Integer.parseInt(st2.nextToken());
            if(start > 0){
                System.out.println(list[end] - list[start-1]);
            } else {
                System.out.println(list[end]);
            }
        }
    }
}
