import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class ProbA {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            String str = br.readLine();
            int one = Integer.parseInt(str.charAt(0)+"");
            int two = Integer.parseInt(str.charAt(1)+"");
            int three = Integer.parseInt(str.charAt(2)+"");
            int four = Integer.parseInt(str.charAt(3)+"");
            int five = Integer.parseInt(str.charAt(4)+"");
            int six = Integer.parseInt(str.charAt(5)+"");
            if(one+two+three == four+five+six){
                System.out.println("Yes");
            } else{
                System.out.println("No");
            }
        }
    }
}
