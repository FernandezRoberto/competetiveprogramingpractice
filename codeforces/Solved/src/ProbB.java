import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class ProbB {
    public static int[] list;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            int num = Integer.parseInt(br.readLine());
            StringTokenizer st = new StringTokenizer(br.readLine());
            list = new int[num];
            for (int j = 0; j < num; j++) {
                list[j] = Integer.parseInt(st.nextToken());
            }
            Arrays.sort(list);
            int sum = 0;
            for (int j = 1; j < list.length; j++) {
                sum = sum + (list[j] - list[0]);
            }
            System.out.println(sum);
        }
    }
}
