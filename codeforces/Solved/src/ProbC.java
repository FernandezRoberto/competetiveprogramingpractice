import javax.print.DocFlavor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class ProbC {
    public static String[] list;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int a = Integer.parseInt(st.nextToken());
            int b = Integer.parseInt(st.nextToken());
            list = new String[a];
            for (int j = 0; j < a; j++) {
                list[j] = br.readLine();
            }
            int min = Integer.MAX_VALUE;
            for (int j = 0; j < list.length; j++) {
                String l1 = list[j];
                for (int k = j+1; k < list.length; k++) {
                    String l2 = list[k];
                    int x = sumStr(l1,l2);
                    if( x < min){
                        min = x;
                    }
                }
            }
            System.out.println(min);
        }
    }

    private static int sumStr(String s1, String s2) {
        int sum=0;
        for(int i=0; i<s1.length(); i++) {
            int asciiValue1= s1.charAt(i);
            int asciiValue2 = s2.charAt(i);

            sum = sum + Math.abs(asciiValue1 - asciiValue2);
        }
        return sum;
    }
}
