import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class ProbD {
    public static int[][] orgMat;
    public static int[][] matArr;
    public static int[][] matAb;
    public static int x;
    public static int y;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            x = Integer.parseInt(st.nextToken());
            y = Integer.parseInt(st.nextToken());
            orgMat = new int[x][y];
            matArr = new int[x][y];
            matAb = new int[x][y];
            for (int j = 0; j < x; j++) {
                StringTokenizer st1 = new StringTokenizer(br.readLine());
                for (int k = 0; k < y; k++) {
                    orgMat[j][k] = Integer.parseInt(st1.nextToken());
                }
                Arrays.fill(matArr[j], -1);
                Arrays.fill(matAb[j], -1);
            }
            fillMat();
            System.out.println(getMaxValue());
        }
    }

    private static int getMaxValue() {
        int max = 0;
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                int v1 = matArr[i][j];
                int v2 = matAb[i][j];
                int v3 = orgMat[i][j];
                int cal = (v1+v2)-v3;
                if(cal > max){
                    max = cal;
                }
            }
        }
        return max;
    }

    private static void fillMat() {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if(matArr[i][j] == -1 || matAb[i][j] == -1){
                    if (matArr[i][j] == -1){
                        calculateArr(i,j);
                    }
                    if (matAb[i][j] == -1){
                        calculateAb(i,j);
                    }
                }
            }
        }

    }

    private static void calculateArr(int i, int j) {
        int aux = j;
        int newj = j;
        int newi = i;
        int sum = 0;
        for (int k = 0; k < aux; k++) {
            sum = sum + orgMat[i][j];
            i++;
            j--;
        }
        for (int k = 0; k < aux; k++) {
            matArr[newi][newj] = sum;
            newi++;
            newj--;
        }
    }
    private static void calculateAb(int i, int j) {
        int aux = j;
        int newj = j;
        int newi = i;
        int sum = 0;
        for (int k = 0; k < (y-aux)-1; k++) {
            sum = sum + orgMat[i][j];
            i++;
            j++;
        }
        for (int k = 0; k < (y-aux)-1; k++) {
            matAb[newi][newj] = sum;
            newi++;
            newj++;
        }
    }
}
