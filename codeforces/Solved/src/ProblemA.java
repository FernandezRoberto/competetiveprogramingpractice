import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class ProblemA {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int size = Integer.parseInt(st.nextToken());
            int red = Integer.parseInt(st.nextToken());
            int blue = Integer.parseInt(st.nextToken());
            System.out.println(solve(size, red, blue));
        }
    }

    private static String solve(int size, int red, int blue) {
        String res = "";
        String[] list = new String[blue];
        for (int i = 0; i < blue; i++) {
            list[i] = "B";
        }
        int i = 0;
        int cont = 0;
        while(true){
            if (i!=blue - 1){
                list[i] = "R" + list[i];
                i++;
            } else {
                list[i] = "R" + list[i] + "R";
                i = 0;
                cont++;
            }
            cont++;
            if (cont >= red){
                break;
            }
        }
        for (String str:list) {
            res = res + str;
        }
        return res.substring(0, size);
    }
}
