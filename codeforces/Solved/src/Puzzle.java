import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Puzzle {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringBuilder sb = new StringBuilder();
        int count = 1;
        while (true) {
            String s = br.readLine();
            if (s.equals("Z"))
                break;
            char[][] data = new char[5][5];
            if (s.length() == 4)
                s += ' ';
            data[0] = s.toCharArray();
            for (int i = 1; i < 5; i++) {
                s = br.readLine();
                if (s.length() == 4)
                    s += ' ';
                data[i] = s.toCharArray();
            }
            int x = 0;
            int y = 0;
            boolean found = false;
            for (int i = 0; i < 5 && !found; i++) {
                for (int j = 0; j < 5 && !found; j++) {
                    if (data[i][j] == ' ') {
                        x = i;
                        y = j;
                        found = true;
                    }
                }
            }
            boolean end = false, isWrong = false;
            while (!end) {
                char[] command = br.readLine().toCharArray();
                for (char c : command) {
                    if (c == '0') {
                        end = true;
                        break;
                    } else if (c == 'A' && !isWrong) {
                        if (x == 0) {
                            isWrong = true;
                        } else {
                            data[x][y] = data[x - 1][y];
                            data[x - 1][y] = ' ';
                            x -= 1;
                        }
                    } else if (c == 'B' && !isWrong) {
                        if (x == 4) {
                            isWrong = true;
                        } else {
                            data[x][y] = data[x + 1][y];
                            data[x + 1][y] = ' ';
                            x += 1;
                        }
                    } else if (c == 'L' && !isWrong) {
                        if (y == 0) {
                            isWrong = true;
                        } else {
                            data[x][y] = data[x][y - 1];
                            data[x][y - 1] = ' ';
                            y -= 1;
                        }
                    } else if (c == 'R' && !isWrong) {
                        if (y == 4) {
                            isWrong = true;
                        } else {
                            data[x][y] = data[x][y + 1];
                            data[x][y + 1] = ' ';
                            y += 1;
                        }
                    } else {
                        isWrong = true;
                    }
                }
            }
            if (count > 1)
                sb.append("\n");
            sb.append("Puzzle #").append(count++).append(":\n");
            if (isWrong) {
                sb.append("This puzzle has no final configuration.\n");
            } else {
                for (int i = 0; i < 5; i++) {
                    for (int j = 0; j < 4; j++) {
                        sb.append(data[i][j]).append(" ");
                    }
                    sb.append(data[i][4]).append("\n");
                }
            }
        }
        System.out.print(sb.toString());
    }
}
