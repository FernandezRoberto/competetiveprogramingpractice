import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class QuestionsAnswers {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine());
        int[] list = new int[num];
        for (int i = 0; i < num; i++) {
            list[i]= Integer.parseInt(br.readLine());
        }
        Arrays.sort(list);
        br.readLine();
        int q = Integer.parseInt(br.readLine());
        for (int i = 0; i < q; i++) {
            System.out.println(list[Integer.parseInt(br.readLine())-1]);
        }
    }
}
