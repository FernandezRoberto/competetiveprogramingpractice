import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class QuickSort {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    String id;
    public static void main(String[] args) throws NumberFormatException, IOException {
        int number = Integer.parseInt(br.readLine());
        int[] sort = new int[number];
        ArrayList<Integer> aux = new ArrayList<>();
        ArrayList<Integer> id = new ArrayList<>();
        ArrayList<Integer> auxId = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            String string = br.readLine();
            String[] stri =  string.split(" ");
            sort[i] = Integer.parseInt(stri[1]);
            aux.add(Integer.parseInt(stri[1]));
            id.add(Integer.parseInt(stri[0]));
        }
        quickSort(sort,0,sort.length - 1);
        for (int i = 0; i < sort.length; i++) {
            int index = aux.indexOf(sort[sort.length-1-i]);
            auxId.add(id.get(index));
            aux.set(index, -1);
        } 
        for(int i = sort.length-1; i >= 0; i--){
            System.out.print(auxId.get(sort.length-1-i)+" ");
            System.out.println(sort[i]);
        }  
    }
    public static int partition(int[] sort, int beg, int end) {
        int left, right, temp, loc, flag;
        loc = left = beg;
        right = end;
        flag = 0;
        while (flag != 1) {
            while ((sort[loc] < sort[right]) && (loc != right))
                right--;
            if (loc == right)
                flag = 1;
            else if (sort[loc] > sort[right]) {
                temp = sort[loc];
                sort[loc] = sort[right];
                sort[right] = temp;
                loc = right;
            }
            if (flag != 1) {
                while ((sort[loc] > sort[left]) && (loc != left))
                    left++;
                if (loc == left)
                    flag = 1;
                else if (sort[loc] < +sort[left]) {
                    temp = sort[loc];
                    sort[loc] = sort[left];
                    sort[left] = temp;
                    loc = left;  
                }  
            }  
        }  
        return loc;  
    }  
    static void quickSort(int[] sort, int beg, int end) {
        int loc;
        if (beg <= end) {
            loc = partition(sort, beg, end);
            quickSort(sort, beg, loc - 1);
            quickSort(sort, loc + 1, end);
        }  

    } 
}
