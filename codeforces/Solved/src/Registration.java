import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Registration {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String st1 = br.readLine();
        String st2 = br.readLine();
        if (st1.equals(st2.substring(0,st2.length()-1))){
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }
}
