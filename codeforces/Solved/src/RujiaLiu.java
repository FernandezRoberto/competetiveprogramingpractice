import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class RujiaLiu {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = br.readLine()) != null) {
            String [] sep = line.split(" ");
            int n = Integer.parseInt(sep[0]);
            int m = Integer.parseInt(sep[1]);
            HashMap<Integer, ArrayList<Integer>> nums = new HashMap<Integer, ArrayList<Integer>>(n);
            String[] numbers = br.readLine().split(" ");
            for (int i = 0; i < n; i++) {
                int num = Integer.parseInt(numbers[i]);
                if (nums.get(num) != null) {
                    nums.get(num).add(i);
                } else {
                    ArrayList<Integer> list = new ArrayList<Integer>();
                    list.add(i);
                    nums.put(num, list);
                }
            }
            for (int i = 0; i < m; ++i) {
                String [] kv = br.readLine().split(" ");
                int k = Integer.parseInt(kv[0]);
                int v = Integer.parseInt(kv[1]);
 
                ArrayList<Integer> temp1 = nums.get(v);
 
                if (temp1.size() < k) {
                    System.out.println("0");
                } else {
                    System.out.println(temp1.get(k - 1) + 1);
                }
            }
        }
    }
}