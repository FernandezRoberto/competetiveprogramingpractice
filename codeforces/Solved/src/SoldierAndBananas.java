import java.util.Scanner;

public class SoldierAndBananas {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int k, n, w;
        k = scan.nextInt();
        n = scan.nextInt();
        w = scan.nextInt();
        System.out.println(bananas(k, n, w));
        scan.close();
    }

    static int bananas(int k, int n, int w) {
        int total = 0;
        for (int i = 0; i < w; i++) {
            total = total + (k * (i + 1));
        }
        return  (total - n < 0) ? 0 : total - n;
    }
}
