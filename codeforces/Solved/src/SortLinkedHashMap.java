import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class SortLinkedHashMap {
    public static void main(String[] args) {
        // Create a list of strings
        HashMap<Integer, Integer> al = new HashMap<Integer, Integer>();
        al.put(1, 2);
        al.put(16, 3);
        al.put(11, 2);
        al.put(20, 3);
        al.put(3, 5);
        al.put(26, 4);
        al.put(7, 1);
        al.put(22, 4);

        Map<Integer,Integer>  sortedMap =  al.entrySet().
                                                stream().
                                                sorted(Map.Entry.comparingByValue()).
        collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    System.out.println(sortedMap);
    }
}
