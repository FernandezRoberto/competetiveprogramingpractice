import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class StonesTable {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine());
        String str = br.readLine();
        int max = 0;
        for (int i = 0; i < num-1; i++) {
            if(str.charAt(i) == str.charAt(i+1)){
                max++;
            }
        }
        System.out.println(max);
    }
}
