import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Stack;

public class StreetParade {
    public static void main(String args[]) throws Exception {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            int number;
            do {
                number = Integer.parseInt(br.readLine());
                if (number == 0) {
                    break;
                }
                Stack<Integer> stack = new Stack<Integer>();
                int[] answer = new int[number];
                int aux = 0;
                String nume = br.readLine();
                String[] num = nume.split(" ");
                for (int i = 0; i < number; i++) {
                    if (answer[0] == 0) {
                        stack.push(Integer.parseInt(num[i]));
                        answer[0] = -1;
                    } else {
                        while (Integer.parseInt(num[i]) > stack.lastElement()) {
                            answer[aux] = stack.pop();
                            aux++;
                            if(stack.isEmpty()) {
                                break;
                            }
                        }
                        stack.push(Integer.parseInt(num[i]));
                    }
                }
                while (!stack.isEmpty()) {
                    answer[aux] = stack.pop();
                    aux++;
                }
                boolean flag = true;
                for (int i = 0; i < answer.length - 1; i++) {
                    if (answer[i] > answer[i + 1]) {
                        flag = false;
                    }
                }
                if (flag) {
                    System.out.println("yes");
                } else {
                    System.out.println("no");
                }
            } while (number != 0);
        } catch (Exception e) {
            return;
        }

    }
}
