import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class StringBuilding {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            String str = br.readLine();
            boolean flag = false;
            int cont = 0;
            if (str.length() > 1 && str.charAt(0)==str.charAt(1)) {
                for (int j = 0; j < str.length()-1; j++) {
                    if (cont == 2){
                        break;
                    }
                    if(str.charAt(j) == str.charAt(j+1)){
                        cont = 0;
                        flag = true;
                    } else {
                        flag = false;
                        cont++;
                    }
                }
            }
            if (flag){
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }
}
