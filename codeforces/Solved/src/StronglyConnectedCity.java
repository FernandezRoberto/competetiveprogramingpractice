import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class StronglyConnectedCity {

    public static int[] str1List;
    public static int[] str2List;
    public static boolean[][] visited;
    public static int cont = 0;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int n = Integer.parseInt(st.nextToken());
        int m = Integer.parseInt(st.nextToken());
        str1List = new int[n];
        str2List = new int[m];
        String str1 = br.readLine();
        String str2 = br.readLine();
        for (int i = 0; i < n; i++) {
            if ((str1.charAt(i) == '<')){
                str1List[i] = -1;
            } else {
                str1List[i] = 1;
            }
        }
        for (int i = 0; i < m; i++) {
            if ((str2.charAt(i) == '^')){
                str2List[i] = -1;
            } else {
                str2List[i] = 1;
            }
        }
        visited = new boolean[25][25];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                cont = 0;
                for (int k = 0; k < n; k++) {
                    for (int l = 0; l < m; l++) {
                        visited[k][l] = false;
                    }
                }
                dfs(i,j,n,m);
                if (cont != n*m){
                    break;
                }
            }
            if (cont != n*m){
                break;
            }
        }
        if (cont == n*m){
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }

    private static void dfs(int i, int j, int n, int m) {
        if(i < 0 || i >= n || j < 0 || j >= m || visited[i][j]){
            return;
        }
        cont++;
        visited[i][j] = true;
        dfs(i,j+str1List[i], n, m);
        dfs(i+str2List[j], j, n, m);
    }

}
