import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class SumDifferences {
    public static int[] list;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int nums = Integer.parseInt(br.readLine());
        StringTokenizer st1 = new StringTokenizer(br.readLine());
        list = new int[nums];
        for (int i = 0; i < nums; i++) {
            list[i] = Integer.parseInt(st1.nextToken());
        }
        Arrays.sort(list);
        long ans = 0;
        for (int i = 1; i < nums; i++) {
            ans += (long) (list[i] - list[i - 1]) * (nums-i)*i;
        }
        System.out.println(ans);
    }
}
