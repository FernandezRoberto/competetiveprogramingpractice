import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class SumRound {
    public static ArrayList<Integer> list = new ArrayList<>();
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            int num = Integer.parseInt(br.readLine());
            int zeros = 0;
            while (num!=0){
                int mod = num%10;
                num = num / 10;
                if(mod != 0){
                    list.add(0, (int) (mod*(Math.pow(10, zeros))));
                }
                zeros++;
            }
            System.out.println(list.size());
            for (int n: list) {
                System.out.print(n + " ");
            }
            System.out.println();
            list.clear();
        }
    }
}
