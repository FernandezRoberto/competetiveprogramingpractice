import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Swaping {

    public static int[] list1;
    public static int[] list2;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            int nums = Integer.parseInt(br.readLine());
            StringTokenizer st = new StringTokenizer(br.readLine());
            StringTokenizer st1 = new StringTokenizer(br.readLine());
            list1 = new int[nums];
            list2 = new int[nums];
            for (int j = 0; j < nums; j++) {
                int n1 = Integer.parseInt(st.nextToken());
                int n2 = Integer.parseInt(st1.nextToken());
                list1[j] = n1;
                list2[j] = n2;
            }
            solve();
        }
    }

    private static void solve() {
        long total = 0;
        for (int i = 0; i < list1.length - 1; i++) {
            long s1=Math.abs(list1[i]-list1[i+1])+Math.abs(list2[i]-list2[i+1]);
            long s2=Math.abs(list1[i]-list2[i+1])+Math.abs(list2[i]-list1[i+1]);
            total = total + Math.min(s1, s2);
        }
        System.out.println(total);
    }
}
