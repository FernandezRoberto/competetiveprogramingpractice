import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeSet;

public class Telefonicos {
    public static TreeSet<Integer> map = new TreeSet<>();
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        String stri = "";
        for (int i = 0; i < num; i++) {
            String s = scan.next();
            map.add(Integer.parseInt(s));
            stri = stri + s;
        }
        ArrayList<String> list = new ArrayList<>();
        for (int i: map) {
            list.add(i+"");
        }
        int sum = 0;
        for (int i = 0; i < list.size(); i++) {
            sum = sum + stri.split(list.get(i), -1).length-1;

        }
        System.out.println(sum);
    }
}
