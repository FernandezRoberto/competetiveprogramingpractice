import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Stack;

class TestClass {
    public static void main(String args[] ) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int number = Integer.parseInt(br.readLine());               
        for(int i = 0; i < number; i++ ) {
            String str = br.readLine();
            Stack<Character> stack = new Stack<Character>();
            for(int j = 0; j < str.length(); j++ ){
                if(stack.isEmpty()) {
                    stack.push(str.charAt(j));
                } else {
                    if(stack.lastElement() == str.charAt(j)) {
                        stack.pop();
                    } else {
                        stack.push(str.charAt(j));
                    }
                }
            }
            if(stack.empty()) {
                System.out.println("KHALI");
            } else {
                String answer = "";
                while(!stack.empty()) {
                    answer = stack.pop() + answer;
                }
                System.out.println(answer);
            }
        }
    }
}
