import java.util.Scanner;

public class TheratreSquare {
    public static void main(String [] args)
    {
       Scanner scan = new Scanner (System.in);
       long n, m, a;
       n=scan.nextInt();
       m=scan.nextInt();
       a=scan.nextInt();
       System.out.println(square(n, m, a));
       scan.close();
    }
    
    static long square(long n, long m, long a){
        long auxm = m / a;
        long auxn = n / a;
        if(m % a != 0) {
            auxm++;
        }
        if(n % a != 0) {
            auxn++;
        }
        return auxm * auxn;
    }
}
