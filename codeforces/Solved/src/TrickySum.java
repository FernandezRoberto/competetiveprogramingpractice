import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TrickySum {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws NumberFormatException, IOException {
        int n;
        n = Integer.parseInt(br.readLine());
        sum(n);
    }

    static void sum(int n) throws NumberFormatException, IOException {
        for (int i = 0; i < n; i++) {
            int number = Integer.parseInt(br.readLine());
            long sum = 0;
            if(number%2 == 0){
                number++;
            } else {
                if(number!=0 && ((number&(number-1)) == 0)) {
                    sum = sum - number;
                } else {
                    sum = sum + number;
                }
            }
            for (int j = 1; j <= number / 2; j++) {
                if(j!=0 && ((j&(j-1)) == 0)) {
                    sum = sum - j;
                } else {
                    sum = sum + j;
                }
                if(number-j!=0 && ((number-j&(number-j-1)) == 0)) {
                    sum = sum - (number-j);
                } else {
                    sum = sum + (number-j);
                }
            }
            System.out.println(sum);
        }
    }
}
