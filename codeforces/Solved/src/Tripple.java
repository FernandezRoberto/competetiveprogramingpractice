import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class Tripple {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        outer:for (int i = 0; i < cases; i++) {
            Map<Integer, Integer> map = new HashMap<>();
            int num = Integer.parseInt(br.readLine());
            StringTokenizer st = new StringTokenizer(br.readLine());
            for (int j = 0; j < num; j++) {
                int n = Integer.parseInt(st.nextToken());
                if(map.containsKey(n)){
                    map.replace(n, map.get(n) + 1);
                    if ( map.get(n) > 2){
                        System.out.println(n);
                        continue outer;
                    }

                } else {
                    map.put(n, 1);
                }
            }
            System.out.println(-1);
        }
    }
}
