import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class TwoGram {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine());
        String str = br.readLine();
        Map<String, Integer> map = new HashMap<>();
        String ans = "";
        int ians = 0;
        for (int i = 0; i < num - 1; i++) {
            String key = str.substring(i, i+2);
            if(map.containsKey(key)){
                map.replace(key, map.get(key) + 1);
            } else {
                map.put(key, 1);
            }
            if(map.get(key) > ians) {
                ians = map.get(key);
                ans = key;
            }
        }
        System.out.println(ans);
    }
}
