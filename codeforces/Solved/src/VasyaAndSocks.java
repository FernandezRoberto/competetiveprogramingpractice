import java.util.Scanner;

public class VasyaAndSocks {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n, m;
        n = scan.nextInt();
        m = scan.nextInt();
        System.out.println(socks(n, m));
        scan.close();
    }

    static int socks(int n, int m) {
        int aux = 0;
        while (n > 0) {
            aux++;
            n--;
            if (aux % m == 0) {
                n++;
            } 
        }
        return aux;
    }
}
