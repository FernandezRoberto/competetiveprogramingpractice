import java.util.Scanner;
public class Watermelon
{
    public static void main(String [] args)
    {
       Scanner scan = new Scanner (System.in);
       String resp="";
       int num;
       num=scan.nextInt();
       resp=watermelon(num);
       System.out.println(resp);
       scan.close();
    }
    
    static String watermelon(int number)
    {
        String resp="NO";
        if(number>3)
        {
            if(number % 2 == 0){
                resp="YES";
            } else {
                resp="NO";
            }
        }
        return resp;
    }
}