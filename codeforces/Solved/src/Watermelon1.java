import java.util.Scanner;

public class Watermelon1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int numero = scan.nextInt();
        String respuesta = "NO";
        if(numero > 3) {
            if( numero % 2 == 0) {
                respuesta = "YES";
            } else {
                respuesta = "NO";
            }
        }
        System.out.println(respuesta);
        scan.close();
    }
    
}
