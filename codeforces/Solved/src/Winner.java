import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.CollationElementIterator;
import java.util.*;

public class Winner {

    public static HashMap<String, Integer> map = new HashMap<>();

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        int max = Integer.MIN_VALUE;
        String wName = "";
        for (int i = 0; i < cases; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            String name = st.nextToken();
            int number = Integer.parseInt(st.nextToken());
            if(map.containsKey(name)){
                int value = map.get(name);
                map.replace(name, value, value + number);
                if(max < map.get(name)){
                    max = map.get(name);
                    wName = name;
                }
            } else {
                map.put(name, number);
                if(max < map.get(name)){
                    max = map.get(name);
                    wName = name;
                }
            }
        }
        System.out.println(wName);
    }
}
