import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Word {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str = br.readLine();
        int lower = 0;
        int upper = 0;
        for (char c: str.toCharArray()) {
            if(Character.isLowerCase(c)){
                lower++;
            } else {
                upper++;
            }
        }
        System.out.println(lower >= upper ? str.toLowerCase() : str.toUpperCase());
    }
}
