import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Wormholes {

    public static List<Edge>[] list;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int cases = Integer.parseInt(br.readLine());
        for (int i = 0; i < cases; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int systems = Integer.parseInt(st.nextToken());
            int wormholes = Integer.parseInt(st.nextToken());
            list = new List[systems];
            for (int j = 0; j < systems; j++) {
                list[j] = new ArrayList<>();
            }
            for (int j = 0; j < wormholes; j++) {
                StringTokenizer st1 = new StringTokenizer(br.readLine());
                int from = Integer.parseInt(st1.nextToken());
                int to = Integer.parseInt(st1.nextToken());
                int weight = Integer.parseInt(st1.nextToken());
                Edge edge = new Edge(to, weight);
                list[from].add(edge);
            }
            if (bellmanFord(list, 0)){
                System.out.println("not possible");
            } else {
                System.out.println("possible");
            }
        }
    }

    public static int num = Integer.MAX_VALUE / 2;
    public static class Edge {
        int to;
        int w;
        public Edge(int to, int w){
            this.to = to;
            this.w = w;
        }
    }

    public static boolean bellmanFord(List<Edge>[] adj, int source){
        int vertices = adj.length;
        int[] dist = new int[vertices];
        for (int i = 0; i < vertices; i++) {
            dist[i] = num;
        }
        dist[source] = 0;
        for (int i = 0; i < vertices - 1; i++) {
            for (int j = 0; j < vertices; j++) {
                for (Edge edge: adj[j]) {
                    int aux = edge.to;
                    if (dist[j] + edge.w < dist[aux]){
                        dist[aux] = dist[j] + edge.w;
                    }
                }
            }
        }
        for (int i = 0; i < vertices; i++) {
            for (Edge edge: adj[i]) {
                int aux = edge.to;
                if (dist[i] + edge.w < dist[aux]){
                    return false;
                }
            }
        }
        return true;
    }
}
