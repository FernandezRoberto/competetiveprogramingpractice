import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Stack;

public class cipherMessage2 {
    public static void main(String args[] ) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str = br.readLine();
        Stack<Character> stack = new Stack<Character>();
        for(int j = 0; j < str.length(); j++ ){
            if(stack.isEmpty()) {
                stack.push(str.charAt(j));
            } else {
                if(stack.lastElement() == str.charAt(j)) {
                    stack.pop();
                } else {
                    stack.push(str.charAt(j));
                }
            }
        }
        /*String answer = "";
        while(!stack.empty()) {
            answer = stack.pop() + answer;
        }*/
        String formattedString = stack.toString()
            .replace(",", "")  //remove the commas
            .replace("[", "")  //remove the right bracket
            .replace("]", "")  //remove the left bracket
            .replace(" ", "")
            .trim();  
        System.out.println(formattedString);
    
    }
}
