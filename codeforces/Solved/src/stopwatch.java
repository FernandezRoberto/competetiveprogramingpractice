import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class stopwatch {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        if(n%2 == 0){
            long ans = 0;
            for (int i = 0; i < n/2; i++) {
                long n1 = Long.parseLong(br.readLine());
                Long n2 = Long.parseLong(br.readLine());
                ans = ans + (n2-n1);
            }
            System.out.println(ans);
        } else {
            System.out.println("still running");
        }
    }
}
