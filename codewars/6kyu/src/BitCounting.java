public class BitCounting {

    public static int countBits(int n){
      int cont=0;
      while(n!=0)
      {
        if(n%2!=0)
          cont++;
        n=n/2;
      }
      return cont;
    }
    
}