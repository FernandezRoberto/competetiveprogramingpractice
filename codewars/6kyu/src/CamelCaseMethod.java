public class CamelCaseMethod {
    public static String camelCase(String str) {
        StringBuffer buffer = new StringBuffer();
        for(String s: str.split(" "))
        {
          if(!s.isEmpty())
          {
            buffer.append(s.substring(0,1).toUpperCase()+s.substring(1));
          }
        }
        return buffer.toString();
    }

}
