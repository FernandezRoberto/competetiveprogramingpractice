public class ConvertToCamelCase {
    
  static String toCamelCase(String s){
    String resp="";
    char car='-';
    for(int i=0;i<s.length();i++)
    {
      if(s.charAt(i)=='-' || s.charAt(i)=='_' )
      {
        s=s.replace(s.charAt(i),s.toUpperCase().charAt(i));
        car=s.charAt(i);
      }
    }
    String[] s1;
    if(car=='-')
      s1=s.split("-");
    else
      s1=s.split("_");
    resp=resp+s1[0];
    for(int i=1; i<s1.length;i++)
    {
      resp=resp+s1[i].substring(0, 1).toUpperCase()+ s1[i].substring(1).toLowerCase();
    }
    return resp;
  }
}
