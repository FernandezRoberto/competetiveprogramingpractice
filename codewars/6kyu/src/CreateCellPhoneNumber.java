public class CreateCellPhoneNumber {
    public static String createPhoneNumber(int[] numbers) {
        String resp="";
        for(int i=0; i<numbers.length; i++)
        {
          if(i==0)
            resp=resp+"(";
          if(i==3)
            resp=resp+") ";
          if(i==6)
            resp=resp+"-";
          resp=resp+numbers[i];
        }
        return resp;
    }
}
