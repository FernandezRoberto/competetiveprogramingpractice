public class DIgitalRoot {
    public static int digital_root(int n) {
        int res = n;
        while(res > 9){
          res = sumarDigitos(res);
        }
        return res;
      }
      public static int sumarDigitos(int numero){
        int res = 0;
        int digito = 0;
        while(numero > 9){
          digito = numero % 10;
          res = res + digito;
          numero = numero / 10;
        }
        res = res + numero;
        return res;
    }
}
