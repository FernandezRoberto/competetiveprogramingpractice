public class DigPow {

    public static long digPow(int n, int p) {
        int resp = 0;
        String aux = n + "";
        int cant = aux.length() - 1;
        int copy = n;
        while (n > 0) {
            resp = (int) (resp + (Math.pow((n % 10), cant + p)));
            n = n / 10;
            cant--;
        }
        if (resp % copy == 0) {
            return resp / copy;
        } else {
            return -1;
        }
    }

    public static void main(String[] args) {
        System.out.println(digPow(92, 1));
        
    }
}