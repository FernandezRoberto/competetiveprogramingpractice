public class DuplicateEncoder {
    static String encode(String word){
      String resp="";
      word=word.toLowerCase();
      boolean bandera= false;
      for(int i=0; i< word.length();i++)
      {
        for(int j=0; j<word.length();j++)
        {
          if(i!=j && word.charAt(i)==word.charAt(j))
          {
            bandera=true;
            break;
          }  
        }
        if(bandera==true)
        {
          resp=resp+")";
          bandera=false;
        }
        else
          resp=resp+"(";
      }
      return resp;
    }
  }