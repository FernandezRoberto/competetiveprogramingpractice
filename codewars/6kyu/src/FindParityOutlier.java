import java.util.ArrayList;
public class FindParityOutlier{
  static int find(int[] integers){
  int resp=0;
    ArrayList<Integer> evens = new ArrayList<Integer>(); 
    ArrayList<Integer> odds = new ArrayList<Integer>(); 
    for(int i=0; i<integers.length; i++)
    {
      if(integers[i]%2==0)
        evens.add(integers[i]);
      else
        odds.add(integers[i]);
    }
    if(odds.size()==1)
      resp=odds.get(0);
    if(evens.size()==1)
      resp=evens.get(0);
    return resp;
  }
}