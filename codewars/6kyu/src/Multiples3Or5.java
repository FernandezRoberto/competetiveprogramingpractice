public class Multiples3Or5 {
    public int solution(int number) {
        int resp=0;
        for(int i=0;i<number;i++){
          if(i%3==0 || i%5==0){
            resp=resp+i;
          }
        }
        return resp;
    }
}
