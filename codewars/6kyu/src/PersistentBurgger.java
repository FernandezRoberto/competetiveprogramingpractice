public class PersistentBurgger {
    public static int persistence(long n) {
        long res = n;
        int cont = 0;
        while (res > 9) {
            res = multiplicarDigitos(res);
            cont++;
        }
        return cont;
    }

    public static long multiplicarDigitos(long numero) {
        long res = 1;
        long digito = 0;
        while (numero > 9) {
            digito = numero % 10;
            res = res * digito;
            numero = numero / 10;
            System.out.println("," + digito);
        }
        res = res * numero;
        return res;
    }
}
