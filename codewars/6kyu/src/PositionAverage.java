public class PositionAverage {
    public static double posAverage(String s) {
      String[] array = s.split(", ");
      int cont = 0;
      int contador = 0;
      int total = 0;
      while(contador < array.length){
        for(int i = contador; i < array.length - 1; i++) {
          for(int j = 0; j < array[i].length(); j++) {
            total++;
            if(array[contador].charAt(j)==array[i+1].charAt(j)) {
              cont++;
            }
          }
        }
        contador++;
      }
      double resp = ((double)cont / total) * 100;
      return resp;
    }
}