public class SortOdd {
    public static int[] sortArray(int[] array) {
        for(int i=0;i<array.length;i++)
        {
          for(int j=i; j<array.length;j++)
          {
            if(array[i]%2!=0 && array[j]%2!=0)
            {
              if(array[j]< array[i])
              {
                int aux=0;
                aux=array[i];
                array[i]=array[j];
                array[j]=aux;
              }
            }
          }
        }
        return array;
    }
}
