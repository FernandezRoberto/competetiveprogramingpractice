import java.util.ArrayList;
import java.util.List;

public class SumConsecutive {
    
    public static List<Integer> sumConsecutives(List<Integer> s) {
      ArrayList<Integer> resp = new ArrayList<Integer>();
      int n = 0;
      for(int i = 0; i < s.size()-1; i++)
      {
        if(s.get(i)==s.get(i+1))
        {
          n=n+s.get(i);
        }
        else
        {
          n=n+s.get(i);
          resp.add(n);
          n=0;
        }
      }
      if(s.get(s.size()-2)==s.get(s.size()-1))
      {
        n=n+s.get(s.size()-1);
      }
      else
      {
        n=n+s.get(s.size()-1);
        resp.add(n);
        n=0;
      }
      return resp;
    }

}