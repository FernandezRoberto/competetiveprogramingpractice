public class SumOfParts {
    //this aproach get accepted by the the site
    public static int[] sumParts(int[] ls) {
        int[] resp = new int[ls.length + 1];
        int sum = 0;
        for(int i = ls.length; i > 0 ; i--) {
          sum = sum + ls[i - 1];
          resp[i-1] = sum;
        }
        resp[resp.length-1] = 0;
        return resp;
    }
    /* first aproach but got a time limit
    public static int[] sumParts(int[] ls) {
        int[] resp = new int[ls.length + 1];
        for(int i = 0; i < ls.length; i++) {
          int sum = 0;
          for(int j = i; j < ls.length; j++) {
            sum = ls[j] + sum;
          }
          resp[i] = sum;
        }
        return resp;
      }*/

    public static void main(String[] args) {
        int[] ls = new int[] {0, 1, 3, 6, 10};
        //System.out.println(sumParts(ls));
        sumParts(ls);
        
    }
}
