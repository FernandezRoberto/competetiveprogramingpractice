public class TenMinWalk {
    public static boolean isValid(char[] walk) {
      boolean resp=false;
      int n=0,s=0,e=0,w=0;
      for(int i=0; i<walk.length;i++)
      {
        if(walk[i]=='n')
          n++;
        if(walk[i]=='s')
          s++;
        if(walk[i]=='w')
          w++;
        if(walk[i]=='e')
          e++;
        
      }
      if(n==s && w==e && (n+e+w+s)==10)
        resp=true;
      return resp;
    }
}