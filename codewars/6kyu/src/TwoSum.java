public class TwoSum {
    public static int[] twoSum(int[] numbers, int target)
    {
      int[] resp= new int[2];
      for(int i=0; i<numbers.length-1; i++)
      {
        for(int j=i+1; j<numbers.length;j++ )
        {
          if(numbers[i]+numbers[j]==target)
          {
            resp[0]=i;
            resp[1]=j;
          }
        }
      }
      return resp; // Do your magic!
    }
}
