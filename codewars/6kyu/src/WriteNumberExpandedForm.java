public class WriteNumberExpandedForm {
    public static String expandedForm(int num)
    {
      String resp="",cont="1";
      while(num!=0)
      {
        int aux = num%10;
        if(aux!=0)
          resp=Integer.parseInt(cont)*aux+" + "+resp;
        cont=cont+"0";
        num=num/10;
      }
      return resp.substring(0,resp.length()-3);
    }
}
