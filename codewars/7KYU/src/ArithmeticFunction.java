public class ArithmeticFunction {
    public static int arithmetic(int a, int b, String operator) {
      if(operator.equals("add"))
        return a+b;
      if(operator.equals("subtract"))
        return a-b;
      if(operator.equals("multiply"))
        return a*b;
      return a/b;
    }
  }