public class CreditCardMask {
    public static String maskify(String str) {
        String resp="";
        for(int i=0;i<str.length();i++)
        {
          if(i<str.length()-4)
          {
            resp=resp+"#";
          }
          else
          {
            resp=resp+str.charAt(i);
          }
        }
      return resp;
    }
}
