public class DisemvowelTrolls {
    public static String disemvowel(String str) {
        String resp="";
        for(int i=0; i<str.length();i++)
        {
          if(str.toLowerCase().charAt(i)=='a'||str.toLowerCase().charAt(i)=='e'||str.toLowerCase().charAt(i)=='i'||str.toLowerCase().charAt(i)=='o'||str.toLowerCase().charAt(i)=='u')
            continue;
          else
            resp=resp+str.charAt(i);
        }
      return resp;
    }
}