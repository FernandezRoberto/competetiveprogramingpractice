public class Factorial {

    public int factorial(int n) {
      int resp=1;
      if(n<0||n>12)
      {
        throw new IllegalArgumentException("Java");
      }
      for(int i=1; i<=n;i++)
      {
        resp=resp*i;
      }
      return resp;
    }
  }