public class FindPerfectSquare {
    public static long findNextSquare(long sq) {
        long var=(long)Math.sqrt(sq);
         if(var*var!=sq)
         {
           return -1;
         }
           long next=(long)Math.sqrt(sq)+1;
         
         return next*next; 
     }
}
