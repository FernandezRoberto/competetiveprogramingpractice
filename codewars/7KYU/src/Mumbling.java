public class Mumbling {
    public static String accum(String s) {
        String resp="";
        for(int i=0;i<s.length();i++)
        {
          int cont=0;
          while(cont<=i)
          {
            if(cont==0)
              resp=resp+(s.toUpperCase().charAt(i));
            else
              resp=resp+s.toLowerCase().charAt(i);
            if(cont+1>i && i!=s.length()-1)
               resp=resp+"-";
            cont++;
          }
        }
        return resp;
    } 
}
