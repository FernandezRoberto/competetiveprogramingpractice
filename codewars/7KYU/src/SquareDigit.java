public class SquareDigit {

    public int squareDigits(int n) {
      String resp="";
      while(n!=0)
      {
        resp=(n%10)*(n%10)+resp;
        n=n/10;
      }
      return Integer.parseInt(resp);
    }
  
  }