public class AreaPerimeter {

    public static int areaOrPerimeter (int l, int w) {
       int resp;
       if(l==w){
         resp=l*w;
       }else{
         resp=(l+w)*2;
       }
       return resp;
    }
}
