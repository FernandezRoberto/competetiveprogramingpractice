public class BasicOperations
{
  public static Integer basicMath(String op, int v1, int v2)
  {
    int resp=0;
    if(op=="+")
      resp=v1+v2;
    if(op=="-")
      resp=v1-v2;
    if(op=="*")
      resp=v1*v2;
    if(op=="/")
      resp=v1/v2;
    return resp;
  }
}