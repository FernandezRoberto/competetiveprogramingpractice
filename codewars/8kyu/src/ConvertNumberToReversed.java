public class ConvertNumberToReversed {
    
    public static int[] digitize(long n) {
        int[] resp= new int[(n+"").length()];
        int cont=0;
        while(n>9)
        {
          resp[cont]=(int)(n%10);
          n=n/10;
          cont++;
        }
        resp[cont]=(int)(n);
        return resp;
      }
}
