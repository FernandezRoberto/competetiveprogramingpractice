public class ConvertToBinary {
    public static int toBinary(int n) {
        String resp="";
        while(n!=0)
        {
          if(n%2==0)
            resp="0"+resp;
          else
            resp="1"+resp;
          n=n/2;
        }
        return Integer.parseInt(resp);
    }
}
