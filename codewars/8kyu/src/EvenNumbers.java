import java.util.ArrayList;
public class EvenNumbers {
  public static int[] divisibleBy(int[] numbers, int divider) {
    ArrayList<Integer> resp = new ArrayList<Integer>();
    for(int i=0; i<numbers.length;i++)
    {
      if(numbers[i]%divider==0)
        resp.add(numbers[i]);
    }
    int[] res=new int[resp.size()];
    for(int i=0; i<res.length;i++)
    {
      res[i]=resp.get(i);
    }
    return res;
  }
}