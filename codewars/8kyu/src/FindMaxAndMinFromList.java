public class FindMaxAndMinFromList {
    public int min(int[] list) {
        int mini=list[0];
        for(int i=1;i<list.length;i++)
        {
          if(mini>list[i])
            mini=list[i];
        }
        return mini;
    }
      
    public int max(int[] list) {
        int maxi=list[0];
        for(int i=1;i<list.length;i++)
        {
          if(maxi<list[i])
            maxi=list[i];
        }
        return maxi;
    }
}
