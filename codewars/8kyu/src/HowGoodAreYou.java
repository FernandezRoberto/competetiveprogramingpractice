public class HowGoodAreYou {
    public static boolean betterThanAverage(int[] classPoints, int yourPoints) {
        int suma=0;
        for(int i=0;i<classPoints.length;i++)
        {
          suma=suma+classPoints[i];
        }
        suma=suma/classPoints.length;
        if(suma>yourPoints)
          return false;
        else
          return true;
    }
}
