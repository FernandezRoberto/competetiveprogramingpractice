public class NthPower {
    public static int nthPower(int[] array, int n) {
        int resp=-1;
        if(n<=array.length-1){
          resp=(int)Math.pow(array[n],n);
        }
        return resp;
    }
}
