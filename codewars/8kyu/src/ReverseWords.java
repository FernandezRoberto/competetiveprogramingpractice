public class ReverseWords{

    public static String reverseWords(String str){
        String[] pal=str.split(" ");
        String resp=pal[pal.length-1];
        for(int i=pal.length-1; i>0; i--)
        {
          resp=resp+" "+pal[i-1];
        }
        return resp;
    }
   }