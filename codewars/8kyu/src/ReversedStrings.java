public class ReversedStrings {
    
    public static String solution(String str) {
        String resp="";
        for(int i=1; i<=str.length(); i++){
          resp=resp+str.charAt(str.length()-i);
        }
        return resp;
      }    
}
