public class SentenceSmash {

  public static String smash(String... words) {
    if(words.length==0)
      return "";
    String resp = words[0];
    for(int i = 1; i < words.length; i++)
    {
      resp = resp + " " + words[i];
    }
    return resp;
  }
}