public class SmallestIntegerFinder {
    public static int findSmallestInt(int[] args) {
      int menor=args[0];
      for(int i=1; i<args.length; i++)
      {
        if(menor>args[i])
        {
          menor=args[i];
        }
      }
      return menor;
    }
}