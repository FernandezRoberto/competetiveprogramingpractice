public class WelcomeToTheCity{
    public String sayHello(String [] name, String city, String state){
      String resp="Hello,";
      for(int i = 0; i < name.length; i++)
      {
        resp = resp + " " + name[i];
      }
      return resp + "! Welcome to " + city + ", " + state + "!";
    }
}